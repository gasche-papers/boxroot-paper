---
template: template.latex
title: |
  Garbage collector roots as linear values
  <!-- Boxroot: mediating between GC- and ownership-based memory management -->
author:
  - |
    | Guillaume Munch-Maccagnoni
    | INRIA
  - |
    | Gabriel Scherer
    | INRIA
date: \today
documentclass: scrartcl
classoption:
#  - DIV=15
# not sure how to avoid pandoc using longtable
# make sure the table does not break across pages
header-includes:
  - \usepackage{natbib}
  - \bibliographystyle{plainnat}
  - \citestyle{authoryear}
  - \usepackage[notextcomp]{stix}
  - \DeclareEncodingSubset{TS1}{?}{0} # fix for option clash for package textcomp, wtf pandoc
  - \usepackage[scaled=0.86]{helvet}
  - \usepackage[scaled=0.95]{inconsolata}
  - \usepackage[utf8]{inputenc}
  - \usepackage{framed}
  - \newenvironment{contribpar}{\begin{center}\begin{minipage}{0.8\columnwidth}\begin{framed}}{\end{framed}\end{minipage}\end{center}}
colorlinks: true
citecolor: Blue4
urlcolor: Blue3
indent: true
numbersections: true
toc: true
toc-depth: 2
secnumdepth: 3
microtypeoptions: expansion=false
abstract: |
    We investigate a programming language concept for roots of garbage
    collector (GC) suitable for interfacing with ownership-based
    memory management. Starting from the idea of a GC root as a
    movable resource in the systems programming languages C++ and
    Rust, we propose:

    - an efficient implementation, with qualitative and quantitative
      results, applied to the foreign-function interface of multicore
      OCaml;
    - a comparison: we found similar concepts in the
      implementation of industrial languages, but little was
      previously documented in the academic literature;
    - a typing discipline in Rust's ownership type system for manipulating
      GC values, ensuring safety at compilation time.

    \noindent Our use-case is the design and implementation of a safe,
    expressive and efficient interface to OCaml in Rust, but we
    believe in a broader interest of the concept of root as a resource
    (or linear value) in the area of memory-management techniques.
---
<!-- TODO: Check TODOs -->

<!--

Requires pandoc-xnos

$ pip install pandoc-fignos pandoc-eqnos pandoc-tablenos pandoc-secnos --user --upgrade

-->

<!--

Pinned handles ? (e.g. OS calls)
 -> Not as crucial, we control when GC can run?
 -> Still the example of lablgtk

Weak references ?

-->


# Introduction

In the implementation of garbage-collected languages, both runtime
implementation and language interoperability are concerned with the
manipulation of (managed) values from a foreign (non-managed)
language, typically C or C++. This raises issues of correctness and
performance. We set out to design an expressive, efficient and safe
interface between the OCaml and Rust programming languages. OCaml is a
functional programming language of the ML family, with a generational
(moving) tracing GC. Rust is a systems programming language which lets
resources (including memory) be managed automatically and predictably,
using ownership concepts inherited from modern C++, with advanced
type-system features for checking memory safety at compilation time.

This choice of languages---OCaml and Rust---led us to approach the
problem from the angle of mixing two resource management techniques:
garbage collection and ownership. As a result, the core contributions
of this paper revolve around a key ingredient for this interface, the
concept of _boxed root_. We identify and exploit through this concept
the complementarity between low-level implementation techniques and
high-level ownership ideas, and explain how this assembles into a safe
and performant FFI between OCaml and Rust.

## Low-level view: FFIs and GC roots

Traditionally, the _foreign function interface_ (FFI) of a managed
language deals with the interaction between non-managed code and the
managed runtime. FFIs must be carefully designed to prevent bugs due
to incorrect usage. Efficiency can also be an important concern since
the reason to resort to foreign code is sometimes (not always) to
write performance-sensitive code.

\defcitealias{JonesHoskingMoss2011GCHandbook}{The GC Handbook}

We focus on expressive FFIs in the sense of enabling the creation,
manipulation and modification of GC-managed values from the foreign
language. This requires mechanisms to keep values alive while they are
used from foreign code, and to update pointers into the GC heap (in
the case of moving GCs). A standard technique is to use an
indirection, with FFIs that expose *handles*: pointers into a table of
values which the collector considers as roots
(\citetalias{JonesHoskingMoss2011GCHandbook}, §11.4). However, the
various implementation techniques used in practice, and the impact on
safety due to the specific discipline the programmer has to follow,
are only partially documented in the academic literature.

We propose an efficient implementation for a kind of handles with few
restrictions, which we call *boxed roots*, for multicore OCaml, which
combines modern concurrent allocation techniques and optimisations
inspired by GC techniques. Our implementation for multicore OCaml is
available as a C library, [`Boxroot`][Boxroot], that extends the OCaml
runtime, with front-ends for C++ and for Rust. It is already in use
inside several OCaml-Rust FFI libraries. It can also be used for
writing C bindings, in particular as it offers better performance and
scalability than current implementations of "global" roots in the OCaml
FFI.

[Boxroot]: https://gitlab.com/ocaml-rust/ocaml-boxroot/


\begin{contribpar}
\emph{Boxed roots} are an efficient implementation of GC handles with similar
interface and absence of restrictions as \verb|malloc|, that exploits a
%<!--synergy-->
complementarity between allocation techniques and GC implementation techniques.
\end{contribpar}

As part of our comparison work, we document for the first time similar
mechanisms present in the FFI of several industrial programming
languages. <!--We document for the first time descriptions of their
implementations and performance comparisons.--> Whilst the `Boxroot`
implementation is conceptually simple, we have found that
implementations of similar mechanisms in industrial languages are not
as performant<!-- TODO: Check claim in regards to updated survey -->.
This could be explained by the fact that unrestricted handles are not
given a central place in other FFI designs.

## GC roots as resources

In our approach to FFIs, GC roots are treated as resources, and
manipulated as so-called “smart pointers” in C++ and Rust:
resource containers obeying an ownership discipline. The typical smart
pointer is the reference-counting pointer, one of the main ways by
which memory is shared and managed automatically in C++ and
Rust---arguably already a form of garbage collection.

For instance, in C++ using our smart pointer `Boxroot`,^[Essentially a
C++ `unique_ptr` with a custom destructor.] list consing for OCaml
values can be implemented as follows:

```c++
/* C++ */
Boxroot cons(Boxroot head, Boxroot tail)
{
    Boxroot cell(caml_alloc_small(2, Tag_cons));
    Field(*cell, 0) = *head;
    Field(*cell, 1) = *tail;
    return cell;
}
```

\noindent <!--In words, a C++ function `cons` is defined, taking two
arguments `head` and `tail` of type `Boxroot`, and returning a value
of type `Boxroot`. --> The function initialises a `Boxroot` with a
call to `caml_alloc_small`, which allocates a new block on the OCaml
heap with suitable size and tag. The function then sets its two fields
to the OCaml values stored inside `head` and `tail`. Correctly, the
type `Boxroot` ensures that `head` and `tail` are seen as roots for
the OCaml GC, should garbage collection trigger during the allocation.
<!--It then returns the cell as the newly-created `Boxroot` after
deleting the roots `head` and `tail`.-->

The C++ type `Boxroot` (and likewise the C type `boxroot`) is
essentially a pointer to a `value const` managed by our allocator,
where `value` is the C type of OCaml values, with a unique reference
to it. The C++ language, through its support for first-class
resources, does the heavy lifting of calling the functions from our C
API for allocating, accessing and deallocating a boxed root,
respectively:

```c
/* C */
inline boxroot boxroot_create(value);
inline value const * boxroot_get_ref(boxroot r);
inline void boxroot_delete(boxroot);
```

\noindent `Boxroot`s are designed to serve all rooting purposes: in
particular they can be passed by value (moved) or by reference, stored
inside data structures, and shared across threads.

In comparison, by following the documented FFI of OCaml, list consing
would be implemented as follows in C (or in C++ without exceptions):

```c
/* C */
value cons(value head, value tail)
{
    CAMLparam2(head, tail);
    CAMLlocal1(cell);
    cell = caml_alloc_small(2, Tag_cons);
    Field(&cell, 0) = head;
    Field(&cell, 1) = tail;
    CAMLreturn(cell);
}
```

\noindent In words, `CAML` macros defined by the OCaml FFI are used
for registering the values `head`, `tail`, and `cell` as roots for the
GC, by storing the addresses of the variables on a shadow stack
managed by the OCaml runtime. This approach follows a *callee-roots*
calling convention: the OCaml values are received directly, and their
address on the stack must be registered as roots by the callee. For
values stored elsewhere than on the call stack, OCaml offers
auxiliary, less efficient mechanisms.

Both managing a shadow stack with the `CAML` macros and using our
smart pointer `Boxroot` require that the programmer follows a certain
discipline. In other words, both techniques make it possible to write
buggy programs. The `Boxroot` approach, however, reduces the problem
of reasoning about roots to one that C++ programmers are familiar with
and that the language supports: resource management.

\subsubsection*{Enforcing the GC discipline statically}

In this paper, we turn to the Rust programming language, as it extends
the C++ model for memory- and resource-management with a type system
that prevents more resource-management mistakes at compilation. We
will recall the idea to use the *lifetime* of a capability to express
and enforce the GC discipline, suggested independently by
\citealp{josephine} and \citealp{caml-oxide}. In this approach,
GC-managed values behave as resources, and can be owned or borrowed.

\begin{contribpar}
We consider the handling of GC roots in FFIs as a resource-management
problem, and build an abstraction for roots as linear values that
combines well with C++- and Rust-style ownership.
\end{contribpar}

\noindent In this context, we have found that:

- How roots are recorded is a key aspect that was not explored in the
  approaches to using ownership to represent the GC discipline. We
  find that the ability to own GC-managed values as roots is important
  in making this approach applicable.

- Efficiency, too, benefits from relying on a type system. Avoiding
  recording locations as roots where unnecessary (to avoid runtime
  costs) is difficult to do by hand given the amount of implicit
  invariants the programmer must keep track of. <!-- Indeed, it
  supports just as easily a finer-grained discipline where one avoids
  recording locations as roots where unnecessary, thereby reducing the
  runtime cost of root management. This would be possible to do
  without the help of a type system, but difficult given the amount of
  implicit invariants the programmer must keep track of. -->

- Interfacing with GCs in Rust reveals limitations of its ownership
  type system. <!-- We point out two limitations that impede on
  handling GCs as idiomatically and safely as we would like. --> We
  contribute a discussion about these limitations, in the hope of
  serving further research on ownership-and-borrowing type systems.

\noindent The safety for FFI disciplines has previously been
investigated from the angle of static analysis, with tools developed
for both the OCaml FFI \citep*{Furr2005FFI} and the Java Native
Interface (\citealp{KondohOnodera2008JNI}), applied to study
real-world code. <!--Though these tools are no longer available, -->An
empirical conclusion of these works was that bugs are indeed common in
FFI-using code. <!-- This is even more true if we take into account
the "expert"---undocumented and error-prone---discipline used in some
OCaml programs, which avoids unnecessary root registration by
reasoning on which program points can trigger the GC. --> In contrast
with bespoke static analysis tools, piggy-backing on an existing type
system may prove easier to popularise and support in the long term.

## High-level view: mixing GC and linear allocation

<!--Taking a higher-level perspective, this paper essentially
describes one way to add an efficient tracing and moving GC to Rust as
a library, improving on earlier attempts to do so. It can also serve
as a stepping stone for future explorations into mixing different
kinds of automatic memory-management techniques in the same
programming language. -->

Automatic memory management in Rust, as in C++, relies on tracking
memory ownership at source level. Ownership-based allocation has
distinct characteristics from tracing GCs, in terms of predictability,
locality and interoperability. It turns out to be closely
related<!--^[See Baker's prediction of move semantics inspired by
linear logic \citep{}.]--> to the concept of linear memory allocation
inspired by linear logic, which back in the 1990's used to promise
“functional programming without a GC” \citep*{Lafont1988, Baker1992},
through prompt deallocation and automatic memory reuse.

Linear allocation with re-use remains attractive to this day, witness
for instance the encouraging performance results of \emph{“functional
but in-place”} programming \citep*{Reinking2021Perceus} which takes
advantage of linearity, however checked at runtime. The independent
progress around *static* linearity, including a better understanding
of the role of _borrowing_ for linearity, and a better understanding
of how to mix linearity with control effects<!-- with the C++/Rust
notion of ownership-->, suggests that this topic is ripe for being
revisited.

One goal with `Boxroot` has been to investigate how GC-managed memory
and linearly-managed memory can be mixed, in a way that reflects how
linear/non-linear type systems mix data types with different kinds of
restrictions (in the style of \citealp{Walker2005a,Tov2011}).
\cite*{Walker2005a} sketches in particular a tutorial language in
which:

- linear values are meant to be deallocated promptly
- non-linear values are meant to be managed by a conventional GC.

\noindent Whilst this proposal is appealing in trying to lift
expressiveness limitations of languages based on linear allocation, it
leaves aside the crucial question of describing how the GC efficiently
keeps track of the values stored inside linear data structures (e.g. a
linear pair of an unrestricted value and a linear value). More
broadly, the debate on linear allocation vs. other memory management
techniques seems to have remained separate from the advances in linear
type systems. This is precisely a gap we aim to fill in this paper.
From this perspective, we find convenient to look at the costs of
boxed roots in the abstract.

\begin{contribpar}
There exists an efficient way to mix (linearly-allocated) linear
values and (GC-allocated) non-linear values, where converting a
non-linear value into a linear one is ``zero-cost'' at runtime, that
is, without added costs compared to the already-paid-for costs of each
memory-management technique.
\end{contribpar}

<!-- TODO: we can simply move the abstract analysis of costs here

More precisely, ... -->

As it turns out, the conversion of a non-linear value into a linear
value, in such a scheme, nevertheless has a necessary runtime
component in the form of root registration. This is an important
observation for linear type systems, as it can rule out certain
systems based on kind coercions disappearing at runtime.

## Context: the OCaml-Rust interface

The specific motivation for investigating the Rust discipline and
building the `Boxroot` library was an industrial user interested in
building a safe FFI between Rust and OCaml. This industrial user asked
for an audit of the existing state of OCaml/Rust bindings from the
perspective of memory-safety and security (for cryptographic code); as
a conclusion of this audit, \citet*{ocaml-rust-audit} suggested that a
safe Rust interface for managing OCaml values in Rust would be
important in order to write safe bindings in Rust.

Following these recommendations, this industrial user sponsored the
development of the [`ocaml-interop`][ocaml-interop] Rust library (main
author: Bruno Deferrari), a library aiming to implement a low-level
but safe interface between OCaml and Rust, initially inspired by an
earlier experiment by \cite*{caml-oxide}. When asked for feedback on
this approach, we decided to build `Boxroot` to offer a better
low-level building block for Rust FFIs. Then, upon our recommandation,
`ocaml-interop` quickly took inspiration from our FFI design and
changed its implementation to sit atop of `Boxroot`.^[We thank Bruno
Deferrari and Jacques-Henri Jourdan for contributions to the
implementation of an early version of Boxroot, and for related
discussions.] Via `ocaml-interop`, the `Boxroot` library is now in use
among several industrial users of mixed OCaml/Rust programs.

[ocaml-interop]: https://lib.rs/crates/ocaml-interop

The current status of `Boxroot` is a C library that hooks into the
stock OCaml runtime to propose our alternative root-registration API,
with front-ends in C++ and Rust. The library is available and free
software.^[At <https://gitlab.com/ocaml-rust/ocaml-boxroot/> and
<https://lib.rs/crates/ocaml-boxroot-sys>.] We have carefully
optimised the library for both OCaml 4 and 5 (multicore). The
development of `Boxroot` has led to various improvements to the C API
of the OCaml runtime to make this possible. In the medium term, we are
hoping to integrate `Boxroot` into the upstream OCaml
runtime.<!--^[<https://github.com/ocaml/RFCs/pull/22>]-->


<!-- ## Contributions -->

<!-- We claim contributions of three different kinds. In particular, taken -->
<!-- together, the three ingredients make up our proposed safe and -->
<!-- efficient FFI between OCaml and Rust. -->


<!-- \paragraph{A programming abstraction of GC roots as linear values} -->

<!-- We investigate a language abstraction for "boxed" GC roots that behave -->
<!-- as a resource (linear value). These roots can be passed and returned -->
<!-- by ownership, borrowed, and stored in data structures obeying -->
<!-- ownership semantics. They can also be sent or shared across threads. -->
<!-- This observation brings together various ideas found in practical -->
<!-- implementations and experiments about GCs in Rust and C++; we expose -->
<!-- it as a clear concept at the core of our performance and -->
<!-- expressiveness claims. We also give an abstract performance argument -->
<!-- that such boxed roots can be zero-cost<\!-- TODO check written -\->; -->
<!-- which brings us to: -->

<!-- \paragraph{GC handles as a concurrent allocation problem} -->

<!-- We consider the problem of efficient and flexible root-registration -->
<!-- mechanisms as an allocation problem. Our solution brings together -->
<!-- techniques from both modern concurrent allocators and garbage -->
<!-- collectors. We propose benchmarks for various situations to support -->
<!-- our efficiency claims, including comparisons against OCaml -->
<!-- root-registration mechanisms and a reimplementation of mechanisms from -->
<!-- the Hotspot/OpenJDK Java runtime.<\!-- TODO: update ? -\-> Since we have -->
<!-- not found in the literature any previous survey of root-registration -->
<!-- mechanisms for FFIs, a secondary contribution of this work is to -->
<!-- document the mechanisms used in various language implementations. -->


<!-- \paragraph{Using Rust's ownership-and-borrowing system for a safe and -->
<!-- efficient FFI} -->

<!-- We explain how Rust's ownership-and-borrowing type system can express -->
<!-- safety disciplines for FFIs. In particular, for handling foreign -->
<!-- managed values, we compare a "callee-roots" convention and a -->
<!-- "caller-roots" convention. The latter is more succinct and idiomatic -->
<!-- for Rust, and made more effective with Boxroot. We also show that the -->
<!-- Rust type system can formalise fine-grained disciplines for managing -->
<!-- foreign values. Lastly, we discuss limitations of the Rust type system -->
<!-- for these purposes. -->


## Outline

TODO <!-- Ce serait vraiment mieux d'inclure des liens dans la section
contributions AMA -->


# Boxroot: unrestricted handles as a concurrent allocation problem

In this section, we describe `boxroot`, an implementation of GC
handles for multicore OCaml: an interface, in the C language, to
declare and manipulate managed values that are seen as roots by the GC.
These `boxroot`s have to be suitable for all programming situations in
a non-managed foreign language, which gives rise to the following
expressiveness and efficiency goals:

- Expressiveness: `boxroot`s are designed to underpin smart pointer
  abstractions, that work similarly to `unique_ptr` in C++ and `Box`
  in Rust. This requires that `boxroot`s must follow standard (e.g.
  `malloc`-like) semantics, with no other constraints than the usual
  resource-management discipline. For instance, it should be possible
  to deallocate a boxroot in random order, from an arbitrary thread,
  and in arbitrary contexts.

- Performance: in order to offer an efficient FFI where a single
  notion of handle suits all purposes, the performance of `boxroot`
  should be good-enough in all use-cases: whether long-lived or
  short-lived, whether local or sent across threads, etc.
  <!-- producer-consumer pattern -->

These constraints led us to investigate efficient unrestricted
handles. Boxed roots are similar to _global references_ in the JNI
\citep*{Liang1999JNI}; what we describe here could serve as an
efficient implementation of global references for a Java runtime. But
we found that, in practice, efficiency was not a requirement of JNI
global roots---as we will see in our comparison with the
Hotspot/OpenJDK Java runtime implementation---the JNI emphasising the
more restrictive but simpler _local references_.


## Description of our implementation

The C type `boxroot` refers to handles for the OCaml GC, which are
pointers to OCaml `value`s, that is, either pointers to a block in the
OCaml heap or pointer-sized tagged integers. These values are kept
alive by the OCaml GC, and are updated when blocks in the heap move.
The Boxroot implementation has two roles: efficiently managing the
allocation of these elements, and efficiently scanning these pools at
the request of the OCaml GC. A natural approach is to store elements
contiguously in fixed-size memory pools.

For the first task, we implemented a custom allocator with a standard
organisation for modern concurrent allocators (e.g.
\citealp{Berger2000Hoard}) around a collection of pools which are
local to each unit of parallelism, with a free list within each pool.
(In multicore OCaml, the unit of parallelism is a *domain* rather than
a thread, details for the multi-threaded aspect are given further
below.) Boxroots are allocated from local pools, and released to their
original pool upon deallocation. Deallocation treats boxroots
differently depending on whether they come from local or remote pools.

\paragraph{Identifying allocated handles during root scanning}

Scanning the roots involves iterating the contents of all allocated
boxroots<!--, calling different functions on each element for minor
collection, major collection and compaction-->. Memory pools, by virtue
of being contiguous, are suitable for efficient traversal for
GC---provided we can efficiently tell allocated objects apart from
free-list elements. Fortunately, the data contained in allocated
objects is not arbitrary: they must at all times contain valid OCaml
values. In particular, allocated objects never contain pointers into
the memory pool. Hence, we know that a slot in a memory pool is
allocated if and only if its value is not the address of an object in
the same memory pool, nor the free-list terminator.

We use pools of size $2^N$ bytes, allocated with an alignment of
$2^N$. <!--The pool metadata, comprising among others the head of its
free list, the number of allocated elements, and pointers to the next
and previous pools, is placed at the start of the pool. In particular,
we can directly retrieve the pool metadata from the handles.--> (In
our case, $2^N=16\,\textrm{KB}$, corresponding to 2032 roots plus
metadata.<!--TODO update-->) The choice of aligning memory pools lets
us test if a slot is an allocated handle of a pool with a single bit
masking and comparison on its value:
$$\mathtt{allocated}(\mathtt{slot},\mathtt{pool}) \coloneq
\texttt{(\ensuremath{\mathtt{pool}} != (\ensuremath{\mathtt{*\!slot}}
\& \textasciitilde(1<{}<\ensuremath{N} - 2)))}$$

\noindent In addition to excluding free-list elements, this test:

- excludes the free-list terminator: we simply use the pool address itself as
  the free-list terminator.
- includes OCaml integers (values with the least significant bit
  set)---whilst not necessary for scanning, this lets us maintain an
  accurate count of allocated elements for an early exit in domains
  that allocate very few roots. <!-- observe that the static bit-mask
  $2^N-2$ is `0b011`…`110`---and -->

\paragraph{Declaring new roots to the OCaml GC}

In order to declare a new set of GC roots, we reuse a low-level
interface to the OCaml garbage collector that accepts arbitrary
higher-order root iteration functions (a feature originally introduced
in the OCaml runtime for the purposes of the abstract machine of the
Rocq proof assistant). The different functions passed by the OCaml
runtime to our root iterators (for promotion by the minor GC, marking
by the major GC, and compaction) are responsible for keeping the
values alive and for updating the pointer values. The multicore OCaml
garbage collector calls our higher-order root iteration functions in
parallel in each domain.

## Generational optimisation

The OCaml garbage collector is generational, with a young generation
processed by a copying collector (the *minor GC*) and an old
generation processed by a mark-sweep collector (the *major GC*). Root
scanning is performed at the start of minor and major GC, the latter
of which can be much more frequent. A natural optimisation is thus to
segregate roots depending on whether they may contain young values. A
previously-scanned root is known to have become old, and therefore it
does not need to be scanned anymore for the minor GC.

The memory pools are managed in one of several local rings, depending
on their *class*: *young*, *old*, or *free*. A young pool may contain
boxroots pointing to the heaps of either generation. An old pool only
contains boxroots pointing in the major heap. With the free pools, we
collect pools as they become empty, available for selection as a new
young pool.

Thus, during minor collection, we only need to scan young pools. At
the end of the minor collection, the young pools, now guaranteed to no
longer point to any young value, are moved into the ring of old pools.
(In particular, a program that does not allocate any root between two
minor collections does not have to scan any pool.)

<!-- todo picking a new young pool? -->

\paragraph{The best time to classify values}

We considered several strategies when allocating a new root:

- unconditionally allocating it in a young pool (our current
  approach),
- allocating old values into old pools directly, and
- recording the young root objects using the remembered set to avoid
  traversing pools during minor GC altogether

The latter two approaches involve a test for the generation of a value
during allocation. Although OCaml supports an efficient test of
whether a value is young, we found that it is more advantageous to
delay this test until scanning the roots for minor collection, for
several reasons:

1. Roots can be short-lived, thus there may be many more roots
allocated than live during collections. Unconditionally allocating
roots in young pools always results in fewer classifications
performed.\footnote{We experimented with a refinement of the
remembered-set approach suggested to us by Stephen Dolan for
short-lived roots: an auxiliary ``young'' free list in each pool can
be used to keep track of the pool elements that have been added to the
remembered set since the last minor GC. This requires a young check
during both allocation and deallocation, with an implementation that
was more complex than our current generational scheme.}
2. Inlining the fast path for the creation and deletion of roots is a
natural approach for use-cases where cache misses are not expected to
be dominant. In this case, we want this fast path to be as small as
possible and to contain only branches that is are statically
predictable.
3. In the case of OCaml, discriminating between old and young values
is more efficient in a tight loop, such as iterating pool elements
during root scanning.

The use-case of local roots (Section @sec:local-bench) gives a
situation where both 1. and 2. are verified in practice.

\paragraph{Creating a new young pool}

When allocating a boxroot, it can happen that the current (young) pool
is full. When this happens, we first look for another young pool that
might be available for allocation<!-- taking remote deallocations into
account-->. If not, then we demote an old pool into a young pool, but
only one which is at least half-empty. Otherwise we prefer to use a
free pool, or to allocate a new pool. This ensures that the extra
scanning of old values during minor collection caused by
demotion---which again is an efficient test---is asymptotically
bounded by the number of new allocations since last minor collection.

<!-- force minor GC when too many young pools? -->

\paragraph{Moving pools during deallocation}

- In order to efficiently find half-empty pools for demotion into
young pools, all the half-empty pool are kept at the front of their
rings. This is achieved by moving pools that become half-empty during
deallocation to the start of their ring.
- Pools that become empty are immediately moved to the free ring,
which is not considered for scanning.

\paragraph{Write barrier}

With this generational scheme, `boxroot` supports the in-place
modification of a root, at the cost of a *write barrier*
`boxroot_modify`:

```c
/* C */
inline bool boxroot_modify(boxroot *, value);
```

`boxroot_modify(*root, value)` is functionally equivalent to
`boxroot_delete(*root)` followed with `*root = boxroot_create(value)`;
in particular, it can possibly fail to reallocate, so it returns
a boolean value. However, when `value` is old, or if the pool
containing `*root` is young, then the value of the handle can be
overwritten directly.

### Multicore implementation

<!-- Hoard:
- Actively-induced False sharing
- Blowup (producer-consumer)
-->


In OCaml 4 and earlier, all threads run sequentially by holding
a master lock each in turn. OCaml 5 relaxes this constraint by
allowing several domains to run in parallel. An OCaml domain is
a collection of (system and/or green) threads, running sequentially by
holding the _domain lock_, OCaml 5's per-domain equivalent of the
master lock. The different threads on the same domain share many data
structures, most notably the same minor heap. Programmers are given
the advice to allocate roughly as many domains as CPU cores to use.

OCaml 5 introduces a new garbage collector implementation
\citep*{SivaramakrishnanEtAl2020}, with a stop-the-world phase that
takes all domain locks to collect minor heaps in parallel (similar in
this aspect to previous efforts by
\citep*{BourgoinEtAl2020OCaml4Multicore}), and an implementation of
a mostly-concurrent mark & sweep collector for the major heap. The
communication between domains is defined by a new memory model
\citep*{Dolan2018}.

Boxroot for OCaml 5 allocates a set of pool rings for each domain.
Each domain-local set of pool rings is protected by the local domain
lock. Every allocation is performed in the domain-local pools. As for
deallocations, each one is classified into: _local_, _cross-domain_,
or _remote_. (We have upstreamed improvements to the OCaml 5
C API to let us perform this classification efficiently.)

- _Local deallocations_ are done on the same domain, while holding the
  domain lock. They are performed immediately without any
  synchronisation being necessary.
- _Cross-domain deallocations_ are coming from a domain which holds
  its domain lock, but is not the domain that allocated the
  boxroot. The typical use-case is sending OCaml values between
  threads, or foreign data structures containing such values.  Since
  _some_ domain lock is held, we know that no interference with
  minor-heap scanning is possible. The deallocated root is pushed on
  a non-local free-list using two RMW atomic operations (no CAS). The
  non-local free list is pushed back on top of the domain-local free list
  at the start of scanning, which takes place during a stop-the-world
  section, when no deallocation can take place.
- _Remote deallocations_ are done without holding any domain
  lock. The typical use-case is for the clean-up of foreign data
  structures that store OCaml values when the domain lock is released,
  e.g. during a panic in Rust. This is rarer, so its performance is
  secondary. To avoid the interference with scanning (without making
  scanning very slow), each pool has a mutex that needs to be locked
  during remote deallocations. This mutex is also locked when
  scanning the pool.^[As an alternative implementation, one could
  avoid locking during purely-remote deallocations by storing the
  metadata about delayed deallocations in a bitmap (taking 32
  additional words in the header for our 16KB pools). By storing this
  metadata separately, one can register remote deallocations in
  parallel with scanning.] The remote deallocation then pushes to the
  non-local free list, just like cross-domain deallocations.

## Comparison, design space

<!--

Whilst widespread---as we witnessed in our survey---GC handles as
described in \citetalias{JonesHoskingMoss2011GCHandbook}
correspond in fact to different abstractions, whose constraints are
dictated by an underlying allocator implementation technique, which
gives rise to a tradeoff between performance, expressiveness, and
safety.

For instance, with the JNI, local references are efficient and
easy to manipulate, and typically implemented with arenas with a
free-list^[in effect a *reap* \citep*{BergerEtAl2002}.]. Global
references are more expressive, since they can be stored in data
structures and shared with other threads, and must be carefully
handled as a resource with a clean-up function.

- vs. JNI

- vs OCaml: callee roots vs. caller roots

Remaining comparisons with other works in terms of expressiveness
(e.g. conservative stack scanning).

-->

<!-- MOVE TO COMPARISON



-->

<!-- OBSOLETE (introducing too many different things at once)

Other approaches rely on controlling when garbage collection happens.
This is the case for the OCaml FFI, which avoids an indirection:
functions receive and return direct pointers into the OCaml heap. A
fairly simple protocol is prescribed in the documentation of OCaml:
the programmer declares at the start of each function which locations
on the stack are to be treated as (*local*) roots for the collector,
before reaching any program point that might have a chance to trigger
a collection. Furthermore, programmers seeking to achieve greater
performance use in practice a more expert discipline that avoids
unnecessary work, by reasoning more precisely on which program points
might trigger the GC. It is expert because it is undocumented and more
error-prone. Again, there is a tension between safer approaches and
more efficient ones.

Boxroot occupies a point in the design space for handles that takes
inspiration from both approaches.

- Expressiveness: our FFI relies on a single multi-purpose abstraction
  of global handles (boxroots), with no restriction regarding its
  usage, similar to JNI global references. Compared to the current
  OCaml FFI, our approach is to pass and return arguments already
  rooted.

- Efficiency: on the one hand, we work to make global handles
  efficient, by adapting modern concurrent allocator techniques in our
  implementation. In addition, we permit the temporary manipulation of
  non-rooted values, which reduces the amount of roots that need to be
  registered, using similar caution as in the OCaml approach.

- Safety: we cast the safety discipline around GC-managed values as a
  resource-management problem, familiar to systems programmers, which
  is concerned with (a datatype abstraction for) GC roots. In
  addition, the manipulation of temporarily non-rooted values, in the
  case of Boxroot, becomes similar to the invalidation of a derived
  reference.

With this point of the design space, we target systems programming
languages with language support for resource-management, such as
modern C++11 and Rust. In comparison, resource manipulation in C, say,
is more delicate and verbose, e.g. by having to use gotos for clean-up
after an error---making it less compelling to do away with local
roots, which are easier to manage as copiable values that clean
themselves up automatically.
-->
<!-- Similarly to the current OCaml FFI, one can devise simple
disciplines to avoid these reference invalidations, or expert ones.
-->


## The problem of cycles

TODO

Importantly, ownership solves the problem of
creating non-collectible cycles<!-- TODO discuss cross-runtime cycles
and explain things in more details in the paper, cf. recent papers(?)
-->.


## Remaining comparisons with other works in terms of expressiveness

e.g. conservative stack scanning


## Implementation of Boxroot {#sec:implem}

<!-- Outline:
1. Description of implementation.
2. Abstract performance argument.
3. Benchmarks (including description of other implementations).
4. Survey of other implementations not included in benchmarks.
-->

## Abstract performance argument

Boxed roots sit at the boundary between two different regions of
memory, one storing linearly-allocated values, handled with explicit
reuse or deallocation, and the other storing non-linear values,
managed by a GC.

We argue that a good implementation can be qualitatively "zero-cost",
in that it does not introduce any overhead over the work that must
be performed by those two disciplines.

- The performance of allocating and deallocating a boxed root should
  be seen as similar to that of allocating and deallocating
  linearly-allocated values with a modern concurrent allocator.

- The performance of scanning boxed roots during GC should be seen as
  similar to that of scanning the same values if they were
  reachable from the GC heap, or better thanks to good memory locality.

We inherit two other qualitative aspects of modern allocators: good
multicore behavior, and the quick reuse of the memory of roots that
were just deallocated.

# Benchmarks

In most cases in current usage, FFI code is not root-intensive and the
performance impact of rooting is likely to be minor; but for
root-heavy benchmarks, boxroot performs similarly or better than other
mechanisms. In particular, the benchmark results in this section
support the following qualitative claims:

1. Boxroot is faster than existing global roots mechanism in OCaml,
   and could replace them. In particular, global root implementations
   have a multicore scalability problem, which is fixed by Boxroot.

2. Allocating a boxroot to store a foreign value has comparable
   performance to allocating an OCaml value on the heap; in other
   words, boxroot indeed appears to be "zero-cost" in our experiments.

3. FFI code differentiates global roots and local roots (restricted to
   a stack-like discipline) for performance reason, because existing
   global root mechanisms are slow -- Java offers a similar
   interface. The performance of Boxroot is good enough, compared to
   local roots, that it is reasonable to expose a simpler FFI
   interface with only boxroots, for example for Rust or C++
   interoperation.

\paragraph{Benchmarking setup}

All benchmarks in this section were obtained with OCaml 5.3 (the most
recent released version), on a common commercial laptop with CPU AMD
Ryzen 7640U, with its CPU frequency fixed at 2Ghz (for reproducibility
and to avoid thermal throttling).

We previously ran those benchmarks with OCaml 4.14 (except for the
multicore benchmark) and a development version of OCaml 5.0, on
another laptop, and obtained similar results supporting the same
qualitative claims.

The measurements are performed by the `hyperfine` utility, which runs
each program 10 times and computes the mean result with a confidence
interval. Our benchmark programs come with an iteration-count
parameter, which we increased until we could observe good stability of
the reported numbers. (Increasing the iteration count internal to the
benchmark is not equivalent to asking `hyperfine` to perform more
runs, as internal iterations share the same runtime state and thus
amortize over potential warmup behaviors of the OCaml runtime, such as
the gradual increase of the major heap to its peak size. We assume
that the steady-state behavior of the benchmark is more relevant to
performance-intensive FFI workloads within a real-world OCaml
program.)


## Global root benchmarks

#### Implementations we compared {#sec:bench-implementations}

Most of our benchmarks are OCaml programs that use a small `Cell`
interface, which exposes a type `'a cell` of a memory cell holding
a value of type `'a`. Cells can be created, dereferenced, and
(unlike standard OCaml references) explicitly deleted. We implemented
this interface via the FFI, where cells are allocated outside the
OCaml heap and their content needs to be rooted, and also directly in
OCaml to provide a performance baseline.

We compare the following implementations:

- `ocaml`: the trivial implementation that does nothing at
  all: it implements cells with `type 'a cell = 'a`, and creation/deletion are
  no-ops. This should be faster than all other versions and provide
  a performance baseline.

- `ocaml-ref` implements cells with `type 'a cell = 'a ref`, that is,
  standard OCaml reference cells. This pure-OCaml version has
  a workflow comparable to the other implementations, with allocation
  and deallocation of cells is handled by the OCaml garbage collector.

- `boxroot` implements cells as a Boxroot handle (tagged as an OCaml
  immediate to be a valid OCaml value).

- `globroots` implements cells as a one-world `malloc`-ed memory
  block, registered as a generational global root, using the mechanism
  provided by the OCaml runtime for FFI users. The OCaml runtime
  uses a skip-list guarded by a global mutex.

In addition, we also implemented the following variants, which help
study specific aspects of the performance results. We will only
include them in some benchmarks, depending on their interest.

- `ocaml-ref-C`: this uses the same implementation as `ocaml-ref`, but
  the operations are implemented in C using the FFI. This gives
  a precise estimate of the overhead of calling C from OCaml in our
  benchmarks, overhead that is also paid by all other FFI solutions.

- `globroots-lf`: we modified the implementation of OCaml's
  generational global roots to use a lock-free skiplist instead of
  a global mutex. (We only include this implementation in
  multi-threaded benchmarks, as it performs worse than `globroots` in
  single-threaded benchmarks.)

- `bx-dll`: we implemented a modified version of `boxroot` to use
  a global doubly-linked list of roots, whose elements are allocated
  and freed using the system allocator directly. This provides
  a simple single-threaded implementation that works surprisingly
  well. (This implementation does not support the multi-threaded
  benchmarks; it performs well, but less well than our contiguous
  pools, so we did not do the extra work of implementing per-domain
  lists and per-domain atomic stacks for remote deallocation.)

- `bx-bitmap`: we implemented a modified version of `boxroot` to use
  a bitmap-based concurrent allocator following the Hotspot
  implementation of global handles (global roots). The Hotspot
  implementation does not contain generational optimizations, but ours
  is generational, it reuses the boxroot design of separating "young"
  pools, which may contain young (and old) values, and "old" pools
  which may only contain old values.

- `bx-boxed`: the `globroots` implementations registers as root
  a word-sized `malloc`-ed block holding the OCaml value, whereas the
  `boxroot` implementation calls `boxroot_create` directly. To control
  that the extra `malloc` does not create an unfair disadvantage for
  `globroots`, we provide `bx-boxed` which implements our `Cell`
  interface with a word-sized `malloc`-ed block containing a Boxroot
  handle.


#### Benchmark programs {#sec:bench-programs}

We selected the following benchmarks:

- `permutations`: a root-intensive sequential benchmark, that is
  computing the list of $n!$ permutations the input list `[1..n]`,
  where lists are represented as linked lists of elements of type `int
  Cell.t`.

- `globroots`: a synthetic benchmark that comes from the OCaml
  compiler testsuite, and stresses the performance of root scanning.

- `parallel-permutations`: a version of the `permutations` benchmark
  that runs N copies in parallel on separate OCaml domains.

- `message-passing`: a concurrent benchmark testing a message-passing
  scenario, where one domain allocates cells and another domains
  deletes them. This exercises cross-domain deallocation.


### `permutations`

<!--
export BOXROOTDIR=/home/gasche/Boulot/ocaml/rust/boxroot
export GITHASH=$(cd $BOXROOTDIR && git log --pretty=reference -n 1 | head -c 7)

export PROG=$BOXROOTDIR/_build/default/benchmarks/perm_count.exe
export RESDIR=benchres/gasche/permutations

mkdir -p $RESDIR
-->

<!--
# N=7, takes about ??? to run
N=7 NITERS=500 \
EXPORT=$RESDIR/$GITHASH-N:$N-NITERS:$NITERS \
  hyperfine \
  -L impl ocaml,ocaml-ref,ocaml-ref-C,boxroot,bx-boxed,bx-dll,bx-bitmap,globroots \
  --export-csv $EXPORT.csv --export-markdown $EXPORT.md \
  --command-name "{impl}" --sort command \
  "CELL={impl} CHOICE=persistent N=$N NITERS=$NITERS $PROG"

# N=8, takes about ??? to run
N=9 NITERS=100 \
EXPORT=$RESDIR/$GITHASH-N:$N-NITERS:$NITERS \
  hyperfine \
  -L impl ocaml,ocaml-ref,ocaml-ref-C,boxroot,bx-boxed,bx-dll,bx-bitmap,globroots \
  --export-csv $EXPORT.csv --export-markdown $EXPORT.md \
  --command-name "{impl}" --sort command \
  "CELL={impl} CHOICE=persistent N=$N NITERS=$NITERS $PROG"

# N=9, takes about ??? to run
N=9 NITERS=10 \
EXPORT=$RESDIR/$GITHASH-N:$N-NITERS:$NITERS \
  hyperfine \
  -L impl ocaml,ocaml-ref,ocaml-ref-C,boxroot,bx-boxed,bx-dll,bx-bitmap,globroots \
  --export-csv $EXPORT.csv --export-markdown $EXPORT.md \
  --command-name "{impl}" --sort command \
  "CELL={impl} CHOICE=persistent N=$N NITERS=$NITERS $PROG"

# N=10, takes about ??? to run
N=10 NITERS=10 \
EXPORT=$RESDIR/$GITHASH-N:$N-NITERS:$NITERS \
  hyperfine \
  -L impl ocaml,ocaml-ref,ocaml-ref-C,boxroot,bx-boxed,bx-dll,bx-bitmap,globroots \
  --export-csv $EXPORT.csv --export-markdown $EXPORT.md \
  --command-name "{impl}" --sort command \
  "CELL={impl} CHOICE=persistent N=$N NITERS=$NITERS $PROG"
-->


<!-- Salient results and internal commentary:

N=7 results (NITERS=500):

| Command       |     Mean [ms] |    Relative |
|:--------------|--------------:|------------:|
| `ocaml`       |  790.1 ± 11.5 |        1.00 |
| `ocaml-ref`   |  862.8 ± 20.4 | 1.09 ± 0.03 |
| `ocaml-ref-C` | 1020.4 ± 29.7 | 1.29 ± 0.04 |
| `boxroot`     |  932.4 ± 42.1 | 1.18 ± 0.06 |
| `bx-boxed`    | 1188.1 ± 45.6 | 1.50 ± 0.06 |
| `bx-dll`      | 1116.3 ± 18.9 | 1.41 ± 0.03 |
| `bx-bitmap`   | 1240.4 ± 16.9 | 1.57 ± 0.03 |
| `globroots`   | 5429.6 ± 35.8 | 6.87 ± 0.11 |


N=8 results (NITERS=100):

| Command       |       Mean [s] |    Relative |
|:--------------|---------------:|------------:|
| `ocaml`       |  1.719 ± 0.067 |        1.00 |
| `ocaml-ref`   |  2.080 ± 0.112 | 1.21 ± 0.08 |
| `ocaml-ref-C` |  2.269 ± 0.057 | 1.32 ± 0.06 |
| `boxroot`     |  2.046 ± 0.099 | 1.19 ± 0.07 |
| `bx-boxed`    |  2.455 ± 0.038 | 1.43 ± 0.06 |
| `bx-dll`      |  2.676 ± 0.112 | 1.56 ± 0.09 |
| `bx-bitmap`   |  2.581 ± 0.068 | 1.50 ± 0.07 |
| `globroots`   | 12.448 ± 0.343 | 7.24 ± 0.34 |

N=9 results (NITERS=10):

| `permutations` (N=9) |      Mean [s] |    Relative |
|:---------------------|--------------:|------------:|
| `ocaml`              | 1.098 ± 0.009 |        1.00 |
| `ocaml-ref`          | 1.275 ± 0.018 | 1.16 ± 0.02 |
| `ocaml-ref-C`        | 1.421 ± 0.049 | 1.29 ± 0.05 |
| `boxroot`            | 1.354 ± 0.016 | 1.23 ± 0.02 |
| `bx-boxed`           | 1.611 ± 0.028 | 1.47 ± 0.03 |
| `bx-dll`             | 1.904 ± 0.025 | 1.73 ± 0.03 |
| `bx-bitmap`          | 1.685 ± 0.017 | 1.54 ± 0.02 |
| `globroots`          | 7.647 ± 0.205 | 6.97 ± 0.20 |

N=10 results (NITERS=10):

| Command       |       Mean [s] |    Relative |
|:--------------|---------------:|------------:|
| `ocaml`       | 12.014 ± 0.053 |        1.00 |
| `ocaml-ref`   | 14.213 ± 0.088 | 1.18 ± 0.01 |
| `ocaml-ref-C` | 15.674 ± 0.069 | 1.30 ± 0.01 |
| `boxroot`     | 15.559 ± 0.114 | 1.30 ± 0.01 |
| `bx-boxed`    | 18.426 ± 0.379 | 1.53 ± 0.03 |
| `bx-dll`      | 20.463 ± 0.085 | 1.70 ± 0.01 |
| `bx-bitmap`   | 18.580 ± 0.104 | 1.55 ± 0.01 |
| `globroots`   | 85.599 ± 0.338 | 7.12 ± 0.04 |


Notes (Gabriel): What I learned so far:

- The relative ratios look fairly different with NITERS=1 and NITERS=5
  or 10; it looks like the program behaves differently when the
  runtime has already been used for independent large computations
  before. I believe that NITERS=10 results are more likely to
  translate to real programs, where boxroot are used in the middle of
  computations that already put the runtime to use.

- I measured performance from N=7 to N=10. The results are overall
  consistent, but the `boxroot` ratio tends to be larger for larger
  values of N (1.18 ± 0.06 for N=7, 1.30 ± 0.01 for N=10).

  My interpretation is as follows: the more memory-intensive the
  benchmark (especially for workloads that don't fit the minor heap),
  the more costly the extra allocations: notice that `ocaml-ref` also
  gets slower relatively to `ocaml` for large N.

  If we look at the ratio between `boxroot` and `ocaml-ref`, it is
  consistently 6%-10% for all values of N, except for N=8.

- I studied the N=8 behavior, where `boxroot` perform much better
  relatively to `ocaml-ref`: for most choices of GC parameters they do
  as well or slightly better (instead of being ~10% slower as for
  other values of N). I don't have an interpretation of this result.

- I looked at the memory usage of the different implementations. They
  exhibit the differences that I would expect (for example bx-dll and
  bx-boxed take more memory than boxroot or ocaml-ref). But this is
  painful to do as `hyperfine` does not know how to collect and
  prepare this information, so I need to run each test
  manually. I propose to not include memory-consumption in the
  benchmarks, as I don't think this is going to qualitatively affect
  program behavior except for locality effects, which are not
  observable in this way either.
-->

The `permutations` benchmark tests a short, allocation-intensive
program, a recursive functions to compute (stricly) the set of `n!`
permutations of an input list:

```ocaml
let rec insert : type a . a -> a list -> a list Choice.t =
  fun elt xs -> match xs with
  | [] -> Choice.return [elt]
  | x :: xs ->
    Choice.choice
      (Choice.return (elt :: x :: xs))
      (let+ xs' = insert elt xs in x :: xs')

let rec permutation : type a . a list -> a list Choice.t = function
  | [] -> Choice.return []
  | x :: xs ->
    let* xs' = permutation xs in
    insert x xs'
```

The non-deterministic monad `'a Choice.t` is implemented as `'a Cell.t list`;
`Choice.choice` performs the concatenation of two lists of cells. The
monadic notation `let+` corresponds to `Choice.map`, and `let*` to
`Choice.bind`. The implementation assumes that `'a Choice.t` values
are used linearily (a `Choice.t` passed as input to one of these
functions is never reused) and will delete the cells of the input
list -- and allocate new cells for the output list.

This function is called on input lists of the form `[1..N]`. Its
running time and memory consumption are both exponential in the `N`
parameter. The resulting program uses cells very intensively, with
a mix of short-lived and long-lived cells, so it will magnify
performance differences between the various implementations.

We observe the following performance results for N=10
(and 5 iterations per run):

| Command       |       Mean [s] |    Relative |
|:--------------|---------------:|------------:|
| `ocaml`       | 12.014 ± 0.053 |        1.00 |
| `ocaml-ref`   | 14.213 ± 0.088 | 1.18 ± 0.01 |
| `ocaml-ref-C` | 15.674 ± 0.069 | 1.30 ± 0.01 |
| `boxroot`     | 15.559 ± 0.114 | 1.30 ± 0.01 |
| `bx-boxed`    | 18.426 ± 0.379 | 1.53 ± 0.03 |
| `bx-dll`      | 20.463 ± 0.085 | 1.70 ± 0.01 |
| `bx-bitmap`   | 18.580 ± 0.104 | 1.55 ± 0.01 |
| `globroots`   | 85.599 ± 0.338 | 7.12 ± 0.04 |

Recall from our description of benchmarked implementations
(@sec:bench-implementations) that `ocaml-ref` uses OCaml references to
represent the type `'a Cell.t`; this is 18% slower than the `ocaml`
baseline where cells are erased and all cell operations are
no-ops.

`ocaml-ref-C` is 30% slower than the baseline. The FFI overhead, which
is apparent by comparing to `ocaml-ref`, is unusually high for this
program. The `Cell` type is used very intensively, and its operations
are very fast, so this is a worst case for FFI overhead -- typically
the time spent on the C side dominates the overhead of spilling
registers and runtime state, but this is not the case here.

`boxroot` has exactly the same performance as `ocaml-ref-C`:
allocating a boxroot has the same performance than allocating an
GC-managed block from the FFI, which supports our claim that `boxroot`
is zero-cost.

The performance of `bx-boxed` shows that allocating an extra
word-sized block via `malloc` does have an observable but small
performance impact. `bx-dll` performs slightly worse again (but not
so bad), possibly due to its higher memory consumption, as each root
becomes a 3-word doubly-linked list cell, also allocated via
`malloc`. `bx-bitmap`, which imitates the Hotspot implementation
(with a general optimization), performs better than `bx-dll` but worse
than `boxroot`.

Finally, we can see that `globroots`, which uses the generational
global roots provided by the OCaml runtime, have a large
overhead. This overhead is not perceptible in programs or libraries
that use a constant number of global roots to store their global
state, but it would make them unsuitable as a general mechanism for an
efficient FFI interface.

To give an estimate of the workload of this program, which is
extremely root-intensive, here are statistics for one run of the
`boxroot` versions with these parameters (N=10, 5 iterations). It
allocates 111MiB of roots in total; and 267MiB on the OCaml heap. Many
pools are freed when they become empty, the peak memory usage of
boxroot pools is 28MiB; and the peak amount of live heap memory is
84Mio. The average pool-scanning time in minor collections is 149
microseconds (the maximum time is 888 microseconds), and the average
pool-scanning time in major collections is 18 milliseconds
(the maximum time is 76 milliseconds; it could be significantly
reduced by implementing incremental scanning of boxroots).


### `globroots`

This benchmark is adapted from the OCaml testsuite. It exercises the
case where there are about 1024 concurrently-live roots, but only
a couple of young roots are created between two minor collections. It
does not perform any OCaml computations or allocations. It forces
collections to occur very often, despite low GC work. So the costs of
root scanning are magnified, they would normally be amortized by OCaml
computations.

<!--
export BOXROOTDIR=/home/gasche/Boulot/ocaml/rust/boxroot
export PROG=$BOXROOTDIR/_build/default/benchmarks/synthetic.exe
export RESDIR=benchres/gasche/synthetic

mkdir -p $RESDIR
-->

<!--
N=500_000 EXPORT=$RESDIR/N:$N \
  hyperfine \
  -L impl ocaml,ocaml-ref,ocaml-ref-C,boxroot,bx-dll,bx-bitmap,globroots \
  --export-csv $EXPORT.csv --export-markdown $EXPORT.md \
  --command-name "{impl}" --sort command \
  "CELL={impl} _build/default/benchmarks/globroots.exe"

To create the "without scanning optim" version, temporarily apply the
following patch to boxroot.c:

```
diff --git i/boxroot/boxroot.c w/boxroot/boxroot.c
index 96b0eb5..1f3fe36 100644
--- i/boxroot/boxroot.c
+++ w/boxroot/boxroot.c
@@ -999,7 +999,7 @@ static inline int scan_pool_aux(scanning_action action, int only_young, void *da
          90% faster for young_hits=10% (random)
          280% faster for young hits=0%
       */
-      if (!only_young || Bxr_is_young(heap_info, v))
+      /* if (!only_young || Bxr_is_young(heap_info, v)) */
         CALL_GC_ACTION(action, data, v, &current->as_value);
     }
     ++current;
```

-->

We observe the following results on this benchmark (with iteration count N=500_000):

| Command                    |      Mean [s] |    Relative |
|:---------------------------|--------------:|------------:|
| `ocaml`                    | 1.415 ± 0.006 |        1.00 |
| `ocaml-ref`                | 1.810 ± 0.042 | 1.28 ± 0.03 |
| `boxroot`                  | 1.539 ± 0.024 | 1.09 ± 0.02 |
| `bx-dll`                   | 1.620 ± 0.035 | 1.15 ± 0.03 |
| `bx-bitmap`                | 1.560 ± 0.037 | 1.10 ± 0.03 |
| `globroots`                | 1.787 ± 0.075 | 1.26 ± 0.05 |

We observe that `globroots` performs well here, similarly to
`ocaml-ref`: there is a fixed number of roots, so the costs of
allocating and deallocating roots are not exercised. The `boxroot`
implementation and its variants perform even better, with `boxroot`
being the fastest conformant implementation of this benchmark --
remember that the `ocaml` baseline has a no-op `Cell` implementation,
so it corresponds to a different workload.

It is not immediate that `boxroot` performs well on this
benchmark. The pools store roots consecutively in memory, so scanning
benefits from good locality. On the other hand, two phenomenons make
scanning slower:

- Pool-based designs, `boxroot` and `bx-bitmap`, have to traverse each
  pool slot to know whether it is allocated or free, so they are at
  a disadvantage compared to list-based designs like `globroots` or
  `bx-dll` which only traverses allocated elements.

  `bx-bitmap` uses the bitmap to find allocated elements faster --
  a single bitmap load gives presence information for 64 elements. For
  `boxroot`, we rely on the optimized `is_allocated` check based on
  pool alignment, and we have some early-exit logic: we know how much
  allocated values are in the pool, and we exit the loop as soon as
  they have all been encountered. (When creating a pool, we initialize
  the free list so that we allocate at the beginning of the pool
  first, so the early-exit work well for partially-filled pools.)

- `bx-dll` and `globroots` both maintain a list with only young
  values. In contrast, `boxroot` and `bx-bitmap` do not perform
  a young check on `boxroot_create`, they delay the check to
  scanning. The check is optimized by hoisting as much logic as
  possible out of the scanning loop, and being mindful of branch
  condition -- it is faster to check if the value is in the minor heap
  before checking if it is a tagged integer, because once we know that
  it is in the minor heap it is very likely to not be an integer.

With both optimisations, `boxroot` has 9% overhead over `ocaml`. If we
disable either the early-exit optimization or the minor-heap-check
optimizations, the overhead becomes 24-25%, similar to
`ocaml-ref`. If we disable both optimizations, the overhead becomes
30%.

### `parallel-permutations`

TODO

### `message-passing`

TODO

## Boxroot and local roots

TODO: this benchmark does not use the `Ref` abstraction, explain how it works.

Specific implementations:

- OCaml's local roots

- `reap`: we implemented a small region allocator using the _reap_
  data-structure introduced in \citep*{BergerEtAl2002} and used in the
  Hotspot implementation of local references (local JNI handles).

## Comparing the concurrent boxroot regimes

TODO Re-measure parallel-permutations with specialized implementations:

- boxroot
- boxroot, always cross-domain)
- boxroot, always remote
- boxroot, always local -- thread-unsafe

Note: the "always local" version is not thread-safe, but it happens to
work correctly in the parallel-permutations benchmark where each
domain is independent, so it never performs any cross-domain
deallocation.


# Safety using Rust

<!-- Outline:
1. Using Rust to make boxroot safe.
2. Comparisons with other works.
-->

<!-- In fact, we show that it is possible to express safely in Rust two
calling conventions, which we call *callee-roots* and *caller-roots*,
corresponding respectively to the OCaml FFI style and the Java FFI
style. However, the latter is more succinct and idiomatic.

In this context:
 - We explore both the callee-roots calling convention of
   \citealp{caml-oxide} (similar to the one of the OCaml FFI) and a
   *caller-roots* convention of \citealp{josephine} (closer in style
   to the Java FFI). We find that the second approach is a better fit
   for Rust
 - We find that and that boxed roots further helps the handling of roots
   as smart pointers further helps making this approach realistic.
-->


## Background about Rust

The Rust language \citep*{Matsakis2014,Anderson2016} is a modern
systems programming language, that eliminates a large class of bugs
typically found in C and C++ programs using various techniques. Rust
achieves this without the need for a GC. Most notably, a notion of
ownership and borrowing of values is used for resource management and
control of aliasing.

For instance, Rust offers a type of _vectors_ (dynamic arrays) similar
to that of C++11, but its type system prevents writing programs
subject to reference or iterator invalidation. Reference invalidation describes a
class of bugs that happen when a reference into a data structure is
used, but the data structure has been changed via an alias, causing
its internal reorganisation. Reference invalidation can be hard to spot
because it results from some action at a distance.

The following Rust program attempts to perform an reference
invalidation:

```rust
    fn main() {
        let mut vec = vec!['a', 'b', 'c'];
        let first = &vec[0];
        vec.push('e');
        let mut _vec2 = vec!['f', 'g', 'h'];
        println!("vec[0] contains {}", first);
    }
```

\noindent In words, a vector `vec` of three elements is allocated. A
reference to the first element of the vector is given the name
`first`. Then a fourth element is added at the end. A second vector
`_vec2` of three elements distinct from the first ones is allocated.
Then one shows the contents of the reference `first`.

Writing the same program in a language such as C++ can (and does)
result in showing the contents of the first element of `_vec2` instead
of `vec`, which has no relationship whatsoever with the meaning of the
program! This happens when adding the fourth element `'e'` reallocates
`vec` elsewhere in memory, and the recycled memory happens to be used
for allocating `_vec2`.

In Rust, this does not happen. The compiler outputs the following
error message:

```
error[E0502]: cannot borrow `vec` as mutable because it is also borrowed as immutable
 --> src/main.rs:4:5
  |
3 |     let first = &vec[0];
  |                  --- immutable borrow occurs here
4 |     vec.push('e');
  |     ^^^^^^^^^^^^^ mutable borrow occurs here
5 |     let mut _vec2 = vec!['f', 'g', 'h'];
6 |     println!("vec[0] contains {}", first);
  |                                    ----- immutable borrow later used here
```

\noindent In words, Rust's type system detects that the reference
`first` is derived from `vec`, may be invalidated by the modification
of `vec` occurring at `vec.push`, and thus cannot be used anymore
after that point. The Rust type system has a notion of _lifetimes_
(or regions) to control aliasing (among others). `first` borrows
immutably from `vec`, and Rust sees that this borrow lives until the
end of the function. It then sees that a mutable borrow of `vec` is
required for adding an element to it (by definition of the
vector interface), while the first borrow still lives, which is
incompatible with the borrowing discipline: _at any point, one can
have either one mutable borrow, or any number of immutable borrows._

## Garbage collection as reference invalidation

The problem with a moving GC for a language such as Rust is that the
GC can invalidate pointers through some action at a distance. It is
a form of reference invalidation; the analogy is sketched in the
following table.

| Vector    | GC heap    | Role                                  |
|-----------|------------|---------------------------------------|
| `v : Vec` | `gc : GC`  | Owns the elements                     |
| reference | GC values  | Borrows direct access to the elements |
| appending | allocating | Can invalidate pointers               |
| index     | *root*     | Invariant name for the elements       |

The basic common idea in \citet*{josephine} and \citet*{caml-oxide}
for dealing safely with a moving GC in Rust is to use design an API to
reference objects in a GC-ed heap that uses Rust lifetimes to prevent
reference invalidation. We pass around a _linear capability_
\citep*{Fluet2006}, `gc`, representing the permission to access the GC
heap (in our case in the form of a mutable or immutable borrow of
a token, denoting the permission to access the runtime data-structures
of the current OCaml domain). This capability lets us borrow direct
pointers into the GC heap, in the form of _non-rooted_ GC values
understood as copiable borrows of `gc`.

Allocation is registered as a mutation of `gc`, and due to this, the
Rust type system prevents us from using the values seen as borrowing
from `gc` after an allocation. In order to access these values after
allocation, we must refer to them through an invariant name, one
preserved across pointer invalidation, like an index in the case of
vectors. A _root_ is a Rust value that gives an invariant name to a
chosen GC value. The notion of root is usually a low-level GC
implementation detail, but now we make it an abstraction in the
language.

## A safe implementation of a relaxed caller-roots discipline

### The runtime capability

As in previous works \citep*{josephine, caml-oxide}, we pass around a
linear capability in the form of mutable or immutable borrows of a
resource of type `Runtime` defined below. This linear capability
represents the ownership of a domain lock, and therefore must remain
local to the thread.

```rust
    pub struct Runtime {
        _private: (),
    }

    impl !Send for Runtime {}
    impl !Sync for Runtime {}
```

\noindent The last lines are pseudo-code for disabling the `Send` and
`Sync` traits for the type `Runtime`, which prevents sending and
sharing this capability between threads.

### OCaml non-rooted values (the C `value` type) {#sec:rust-value}

The next type represents OCaml values (compatible with the type
`value` in C) that are always safe for reading transitively. In
particular they immutably borrow from the capability if needed to
safely read the data (i.e. if they transitively point to the OCaml
heap).

```rust
    #[derive(Copy, Clone)]
    #[repr(C)]
    pub struct Value<'a> {
        raw: isize,
        _marker: PhantomData<&'a Runtime>
    }
```

\noindent Since they must always be safe to read, these values must be
invalidated when an allocation takes place if necessary (this can move
or deallocate the value if it resides on the OCaml heap). This is the
role of the lifetime annotation.

Accessing a field of a value gives a value with the same access
permission:

```rust
    pub fn Value::get<'a>(self: Value<'a>, offset: usize) -> Value<'a>
```

\noindent We do not know _a priori_ whether the value is a block and
if so, what its length is. Let us just decide that the function panics
(raises an exception) if the value does not have the right memory
layout (let it test whether it is a block, and read the length from
the block header). In this paper, we do not address the question of
matching OCaml types to Rust types in terms of memory layout: this is
a different question from that of co-existing with the GC, the one
that interests us here. This question aside, we can implement this
function safely thanks to the lifetime discipline.

#### Examples

```rust
    pub fn val_long(n: isize) -> Value<'static>
```

\noindent This function signature expresses converting a Rust integer
into an immediate OCaml value (the macro `Val_long` in C). Since it is
always safe to read from an immediate value, the lifetime is
unconstrained (`'static`).

```rust
    pub fn caml_alloc<'a>(gc: &'a mut Runtime, size: usize, tag: u8) -> Value<'a>
```

\noindent This function signature expresses allocating a block of
size `size` (in words) and tag `tag` in the OCaml heap. It requires a
mutable borrow of the capability, and returns a value with the same
lifetime. Therefore calling `caml_alloc` invalidates existing
non-rooted pointers into the OCaml heap, and calling it a second time
invalidates the value resulting from the first call (which should
better have been stored in a root in the meanwhile).

#### Note

The type `Value<'a>` is intended to be copiable, but in the signature
above, `'a` refers to a mutable borrow. Therefore, Rust believes that
`Value<'a>` borrows mutably from `'a`. This is a well-known limitation
of the Rust type system which makes it believe that the value returned
from `caml_alloc` mutably borrows from the capability, rather than
immutably. This can force introducing more roots than necessary, and
unfortunately this pattern is easy to come across in our context
(Section @sec:limitations).

### Non-allocating functions

We can now write and call functions that receive OCaml values as
arguments, but that do not allocate on the OCaml heap:

```rust
    fn max<'a>(x: Value<'a>, y: Value<'a>) -> Value<'a>
    fn max2<'a,'b,'c>(gc: &'a Runtime, x: Value<'a>, y: Value<'a>) -> Value<'a>
```

<!-- TODO: discuss a bit more why 'max' is interesting, how could it access the runtime?
     (access custom operations for comparison?) -->

The first function does not take a `gc` parameter at all, it can only access the parts of the OCaml heap reachable from `x` and `y`. The second function borrows the runtime capability, immutably: it can call FFI functions are only safe when the runtime lock is held, but it cannot mutate the heap by allocating.

It is tempting to consider the following signature for a function that accepts values as arguments and also allocates (in this example, it allocates the pair of `x` and `y`):

```rust
    /* Discouraged */
    fn pair_wrong<'a,'b,'c>(gc: &'a mut Runtime, x: Value<'b>, y: Value<'c>) -> Value<'a>
```

This signature is too restrictive: `'b` and `'c` cannot be derived from the same `gc` parameter, as mutable borrows are exclusive. In our case, this means that `x` and `y` must be allocated outside of the OCaml heap, that is, they must be immediate or static.

### Rooting with Boxroot

To interleave allocations and value access, we must introduce a way to register GC roots. We now define a type of boxroots:

```rust
    #[repr(C)]
    pub struct Root {
        cell: std::ptr::NonNull<std::cell::UnsafeCell<Value<'static>>>
    }
```

<!-- TODO: centralize implementation questions in a subsection. -->

\noindent In words, a `Root` is a non-null pointer to an `UnsafeCell`
containing a `value`. The memory layout of `Root` is the same as the C
type `value *`.

The Rust type `UnsafeCell` tells the compiler that this memory
location is possibly aliased and could be changed by someone else (in
our case, by the OCaml GC, when promoting young values). This is
necessary to avoid over-eager optimisations Rust might do based on its
deep understanding of (non-)aliasing of variables.

We offer the following API on the `Root` type:
```rust
    pub fn Root::create<'a,'b>(gc: &'a Runtime, val: Value<'b>) -> Root
    pub fn Root::get<'a,'b>(self: &'b Root, gc: &'a Runtime) -> Value<'a>
    pub fn Root::drop(&mut self)

    unsafe impl Send for Root {}
    unsafe impl Sync for Root {}
```

* `create` wraps `boxroot_create`. It requires the domain lock being
  held, and a readable value. It returns a boxroot. (In case
  `boxroot_create` returns `NULL`, it panics.)
* `get` wraps `boxroot_get`. It requires the domain lock to be held,
  and returns a value that borrows the capability passed in argument.
* `drop` is used to implement the trait `Drop` for Root (to implement
  the RAII pattern). Its use has no constraint apart from ownership of
  the argument, not even the domain lock being held.
* The `Send` and `Sync` traits let boxroots be sent and shared accross
  threads. The ownership of a boxroot can for instance be shared
  between threads by inserting it inside an atomic reference-counted
  pointer.

### The caller roots the arguments

This leads us to a first solution for writing a function that can
allocate on the OCaml heap while accepting additional values as
arguments.

```rust
    fn pair_consume_roots<'a>(gc: &'a mut Runtime, x: Root, y: Root) -> Value<'a>
```

Notice that a `Root` does not carry a lifetime. Being independent from
the `gc` capability is its main characteristic. The above signature
can therefore accept any `Root` as argument, and we have seen that we
can create a root from any `Value`. This is a proper solution to our
problem.

However, with this type the caller transfers ownership of the roots to the callee, and does not regain them back after the call. If it needs the values again later, it might have to register them in several roots. Rust does not make it easy to return permission to the caller, it is idiomatic to use borrowing instead:

```rust
    fn pair_borrow_roots<'a,'b,'c>(gc: &'a mut Runtime, x: &'b Root, y: &'c Root) -> Value<'a>
```

\noindent This works but has several drawbacks:

- The memory layout of `&Root` is essentially `value**`: we pay the
  cost of a double indirection.
- This function only accepts values rooted with `Root`. In practice,
  though, `pair_borrow_roots` does not care how the value is kept alive. Nothing
  should prevent it from accepting values that are static or immediate
  (without forcing those to be rooted needlessly), or values rooted by
  other means.

### `ValueRef`, a type of OCaml values with reference semantics {#sec:rust-valueref}

Alongside the `Value` type which has value semantics, we introduce the
type `ValueRef` which represents OCaml values with reference
semantics. It is essentially a `volatile value *` in C.^[In the
current implementation of the OCaml 5 runtime, `volatile value` is a
proxy for an `_Atomic(value)` accessed via relaxed loads and stores.]

```rust
    #[repr(C)]
    pub struct ValueCell {
        atomic: AtomicIsize
    }

    type ValueRef<'a> = &'a ValueCell;
```

In words, a `ValueRef` is a reference to a `ValueCell`. A `ValueCell`
is an atomic `value`. The atomicity plays two roles:

- it lets us define `ValueRef`s that point into the OCaml heap. To
  satisfy the OCaml memory model, the loads from the OCaml heap must
  be relaxed atomic.\footnote{A bit stronger than a relaxed load
  actually, but this is a different question.}
- it replaces `UnsafeCell` in its role, letting the compiler know that
  the contents can change at the whim of the GC.

A `ValueCell` has no lifetime annotation. Instead, the lifetime of the
reference determines a `ValueRef`'s validity.

A `ValueCell` is meant as a polymorphic container, which can represent
values that are rooted or not, allocated the OCaml heap or not
(static), etc. Examples:

```rust
    pub unsafe fn Value::get_ref<'a>(self: Value<'a>, offset: usize) -> ValueRef<'a>
    pub fn Root::get_ref<'a>(self: &'a Root) -> ValueRef<'a>
```

<!-- [Gabriel]: why is Value::get_ref unsafe? -->

\noindent The two functions compile merely to identity/pointer
arithmetic.

* In the first case, a pointer to the field `offset` is returned,
  possibly inside the OCaml heap. Its lifetime is the same as that of
  the original value. Thus, if the reference points inside the heap,
  it will not be possible to access it after an allocation.
* The second one is `boxroot_get_ref`. The `ValueRef` points to a
  rooted location, and its lifetime is that of the boxroot. If an
  allocation happens, the value is kept alive by the GC, and the
  location kept up-to-date, so the `ValueRef` keeps on living.

A `ValueCell` can be dereferenced only when holding the domain lock.

```rust
    pub fn as_val<'a,'b>(gc: &'a Runtime, val: ValueRef<'b>) -> Value<'a> {
```

Reading the value pointed-to by the `ValueRef` produces a non-rooted
value. Its lifetime is thus constrained by the capability. Indeed, we
have just seen that `'b` can outlive `'a` in reasonable situations
(case where the `val` is a root), and therefore cannot be used as the
lifetime of value.

### Expanding the caller-roots protocol

We implement the `Deref` trait in such a way that references to roots or values are convertible into `ValueRef`.

```rust
    impl Deref for Root {
        type Target = ValueCell;
        fn deref(&self) -> ValueRef { .../* boxroot_get_ref */ }
    }

    impl<'a> Deref for Value<'a> {
        type Target = ValueCell;
        fn deref(&self) -> ValueRef { .../* identity */ }
    }
```

\noindent This trait is treated specially by Rust, and used to
automatically convert `&root` (when `root : root`) or `&val` (when `val : Value<'a>`) to the type `ValueRef`.

It is now possible to rewrite `max2` and `pair_borrow_roots` above as follows:

```rust
    fn max3<'a,'b,'c>(gc: &'a Runtime, x: ValueRef<'b>, y: ValueRef<'c>) -> T
    fn pair<'a,'b,'c>(gc: &'a mut Runtime, x: ValueRef<'b>, y: ValueRef<'c>) -> T
```

\noindent where `T` can be `Value<'a>` and `ValueRef<'a>`. Using `Value<'a>` as return type is conceptually simpler, but consistently using `ValueRef<'a>` would let us dispense of exposing `Value` altogether for simplicity.

### Summary

* `Value<'a>` has value semantics, it can represent static and
immediate values, or pointers to blocks allocated on the heap (in
which case the lifetime `'a` is bounded by the next allocation).

* `ValueRef<'a>` has reference semantics. It is a polymorphic type
that can abstract a variety of pointers to OCaml values: immediates,
static values, non-rooted values, values inside the OCaml heap, roots
(boxed, local, global), maybe more. The lifetime `'a` may or may not
be bounded by the next allocation.

To more accurately represent the OCaml memory model, we can further
define:

- other types with value semantics (e.g. a type for values that are
are still being initialized, `ValueUnshared`, and one for the encoded
raised exceptions returned by some of OCaml's runtime functions,
`ValueExn`)
- other types with reference semantics (e.g. types for mutable and
atomic locations in the OCaml heap, `&MutCell` and `&AtomicCell`).

We have seen 3 kinds of functions that manipulate OCaml values,
distinguished according to how they depend on the capability
capability:

1. $\boldsymbol{\emptyset}$: No runtime access (`fn f(...) -> T`). The
   function can access the heap via supplied arguments, but it cannot
   call runtime functions nor dereference `ValueRef`s.

2. **R**: Read-only runtime access (`fn f(gc: &Runtime, ...) -> T`).
   The function can call runtime functions which do not trigger a GC,
   and it can dereference `ValueRef`s.

3. **RW**: Read-write runtime access (`fn f(gc: &mut Runtime, ...) -> T`).
   The function can call allocation functions as well as release and
   re-acquire the runtime lock.

\noindent In each case, the return type `T` can contain `Value`s,
`ValueRef`s, and `Root`s.

The following table summarises which function signature accepts which
kinds of arguments.

|                                | $\emptyset$ | R   | RW                                    |
|--------------------------------|-------------|-----|---------------------------------------|
| Types with value semantics     |             |     |                                       |
| `Value<'a>`                    | Yes         | Yes | Yes (if static or immediate)          |
| Types with reference semantics |             |     |                                       |
| `ValueRef<'a> = &'a ValueCell` | No*         | Yes | Yes (if rooted, static, or immediate) |
| Rooted values                  |             |     |                                       |
| `Root`                         | No*         | Yes | Yes                                   |

\noindent *: Not in a useful way

### Example

One can easily pass immediates to an allocating function:
```rust
    let imm : Value<'static> = val_long(42);
    pair(gc, &imm, &imm);
```

\noindent If you forget that one must pass references, you can count on Rust
error messages to help you:
```
745 |     pair(gc, imm, imm);
    |              ^^^
    |              |
    |              expected `&ValueCell`, found struct `Value`
    |              help: consider borrowing here: `&imm`
```

\noindent One can also easily pass non-rooted values to non-allocating functions.
```rust
    let val = caml_alloc(gc, 1, 0);
    max(imm, val); // by value
    max3(gc, &imm, &val); // by reference
```

It would not possible to pass non-rooted values to allocating functions:

```rust
769 |     let val = caml_alloc(gc, 1, 0);
    |                          -- immutable borrow occurs here
...
775 |     pair(gc, &imm, &val);
    |     ----^^^^^^^^^^^^^^^^
    |     |
    |     mutable borrow occurs here
    |     immutable borrow later used by call
```

\noindent This is the same error message that we had with iterator
invalidation. The programmer gets around this by introducing a root:
```rust
    let root = Root::create(gc, val);
    let val2 = pair(gc, &imm, &root);
    max3(gc, &val2, &root);
```

Lastly, at this point, `val` is not longer live:
```rust
    //pair(gc, &imm, &val); // Compilation error: good
```

## Rust limitations {#sec:limitations}

### Return-value pollution

The attentive reader will have noticed that there is still something
wrong with the previous example. <!-- TODO: more details: signature, etc. -->

```rust
let imm : Value<'static> = val_long(42);
pair(gc, &imm, &imm);
let val = caml_alloc(gc, 1, 0);
let root = Root::create(gc, val);
let val2 = pair(gc, &imm, &root);
```

As we have mentioned in
@sec:rust-value, the Rust type system has a vexing limitation. `val`,
the result of `caml_alloc`, is (arguably erroneously) understood by
Rust as borrowing from `gc` _mutably_. Consequently, it should not
even be possible to call `Root::create` with both `gc` and `val` as
arguments, even if `gc` is passed as an immutable borrow.

<!-- TODO: rework Root::create to not ask the gc capability from the
start. Here, in this new version, we could say "it would be natural to
have Boxroot::create take the gc capability, but it does not work,
here is why...". -->

There are various ways around this, but there is nothing fundamental
about it.

1. It is possible to break the lifetime dependency inside `caml_alloc`
   by (unsafely) casting the lifetime away and re-borrowing the
   capability immutably. Using domain knowledge, we know that this is
   safe. Then, using the expressive macro system of Rust, we can try
   to automate this trick.

2. Alternatively, it makes sense to not require the `gc` capability
   for the boxroot operations. It can be replaced with a dynamic check
   that the domain lock is held, which panics (raises an exception).
   This works well because when we decide to root a value `Value<'a>`,
   its validity usually means that we already hold the domain lock,
   so a dynamic check would not fail. Sometimes we pass an immediate
   or static value, but then we can often avoid rooting.

3. If we are asked to root a value that is outside the OCaml heap, we
   can avoid panicking if the domain lock is not held and instead
   create a "fake" root: they do not need to be scanned and can
   therefore be `malloc`ed separately using the system allocator. In
   this case, the need to be able to discriminate efficiently real
   roots from fake roots during deallocation can make the
   implementation more complex.


<!--
    Before:

       caml_alloc(&mut 'a Gc) -> Value<'a>
       caml_named_value(&'a Gc) -> Value<'static>
       Root::create(&'a Gc, Value<'a>) -> Root
       Root::create_remote(Value<'a>) -> Root
       Root::get(&'a Gc, &Root) -> Value<'a>

       conflict between Root::create and live unrooted values.


    [Gabriel] Alistair suggested splitting the Runtime and the Gc capabilities:
     - functions that need the runtime lock can take an immutable Runtime borrow
     - functions that invalidate unrooted values still take a mutable Gc borrow

    How could that possibly work?

       &'a Runtime, &'b Heap with 'b : 'a

       caml_alloc(&Runtime, &mut 'a Heap) -> Value<'a>
       caml_named_value(&Runtime) -> Value<'static>
       Root::create(&Runtime, Value<'a>) -> Root
       Root::create_remote(Value<'a>) -> Root
       Root::get(&Runtime, &'a Heap, &Root) -> Value<'a>

       conflict between Root::get and live unrooted values


       v = caml_alloc(gc);
       w = Root::get(gc, r);
       z = caml_alloc2(v, w);

       v = caml_alloc(gc);
       v' = Root::get(gc, Root::create(rt, v));

    There still seems to be an issue with Root::get:
    - the lifetime of the unrooted value must be at most
      the lifetime of the current GC state
    - this seems incompatible with having a freshly allocated
      value in the environment

    Note: maybe we could simplify the arguments (&Runtime, &'a Heap)
    and turn it into just (&'a Heap) if the Heap is always
    a sub-capability of the Runtime.

    Gabriel: this seems to work well to depollute the functions that
    need the runtime lock but do not access the heap at all. On the
    other hand, we are still stuck for functions that need read-only
    access to the heap, like Root::get.

-->

This is not the only limitation of Rust that we have encountered
during our research.

### Container pollution

Another limitation which we have encountered is with implementing the
destructor for boxroots. We are treating specially inside the
implementation of Boxroot the case where the domain lock is not held
during deallocation. This is treated as a remote deletion. One reason,
the good one, is that one purpose of Boxroot is to store OCaml values
inside foreign data structures. In this use-case, one might
legitimately want to be able to clean-up the structure anytime. This
means deleting the root without necessarily holding the domain lock.

There was also another reason for this choice. Rust does not seem to
offer a good way of ensuring that the destructor can only be called
while the capability is still live (if necessary by imposing
constraints on where the roots can live). Not having any functional
constraint on the deallocation of boxroots sidesteps this issue.

However this hides a more general limitation: it does not seem
possible to obtain the capability inside of a destructor, even when
this destructor runs while the capability is in scope and borrowable.
Thus, this causes some difficulty if the release of a resource
requires using the OCaml runtime (for instance, calling some OCaml
code).

Again there are several workarounds, none ideal:

1. Avoiding the automatical destruction of resources whose clean-up
   needs the capability. By manually destroying them instead, it is
   possible to pass the capability as an argument to the manual
   destructor.

2. Providing a function to create a new capability out of nothing
   inside the destructor. This is highly unsafe, but destructors are
   currently a dark corner of Rust where other unsafe manipulations
   take place currently.^[Cf. `#[may_dangle]`.] A refinement of this
   idea is to dynamically check whether the domain lock is held, and
   wrap the new capability in an option type (this is still unsafe).

Ideally, one should be able to hold a borrow of the capability inside
the resource that is going to be destroyed, for later use. But holding
such a borrow in the Rust type system will prevent any allocation
until this resource is destroyed.

This limitation is related to a failure by Rust to distinguish the
borrowing of a container, from the borrowing of its contents. For
instance:

```rust
    let mut x = Box::new(2);
    let x_ref : &Box<u8> = &x;
    *(x.as_mut()) = 3;
    println!("x contents: {}", x_ref.as_ref());
```

\noindent In words, a memory cell is allocated containing the
integer 2. A reference to this memory cell is taken, then the
_contents_ are modified to hold the integer 3. One then tries to print
the contents of the memory cell through its reference.

Rust fails with the following error message during compilation:

```
error[E0502]: cannot borrow `x` as mutable because it is also borrowed as immutable
 --> src/main.rs:4:6
  |
3 |     let x_ref : &Box<u8> = &x; // (1)
  |                            -- immutable borrow occurs here
4 |     *(x.as_mut()) = 3; // (2)
  |      ^^^^^^^^^^^^ mutable borrow occurs here
5 |     println!("x contents: {}", x_ref.as_ref());
  |                                -------------- immutable borrow later used here
```

\noindent Rust understands that the mutation of the contents
invalidated the reference to the container. Arguably, though, it would
make sense for the program to print the new contents, 3.

In our problem, we replace `x_ref` with the creation of a resource
that borrows from the `gc` capability, and we replace `println` with
the call to the destructor of the resource (inside which we try to
access the capability via its borrow). <!--Under this analogy,
allocation should be understood as mutating the _contents_ of `gc`
rather than `gc` iself. speculative-->

It should be noted that both this limitation and the previous one have
been noticed separately in the Rust community, and several proposals
have been drafted to attempt to relax the discipline. However, no
convincing solutions have been proposed yet.

### Our summary

The Rust API of Boxroot expresses ownership of the OCaml runtime lock through a Rust lifetime. This works well and seems expressive enough to safely implement most FFI code. However, in doing so we have encountered some limitations, where the lifetime rules implemented in Rust directly contradicted our expectations for Boxroot, and our mental model of lifetime ownership.

We identify two different causes for these mismatches:

Inexpressive surface.
: Rust borrowing rules orchestrate between shared read-only access and unique read-write access to resources, with a clear informal correctness criterion that both modes are mutually exclusive. But it is *not* the case that any ownership discipline that respects this exclusion principle can be encoded in the Rust surface syntax.  Indeed, the Rust surface language compromises on expressiveness of the lifetime discipline to make the rules easier to use, enforce, explain, etc. If there was a more expressive core language of ownership that Rust programs desugar into (think: the GHC Core of ownership), we could wonder whether our domain-specific rules are expressible at that level.

Incompatible axioms.
: Rust does not implement a "free theory of ownership". Its lifetime-checking rules embed specific axioms that were co-designed with the specific needs of memory handling as exposed via the Rust standard library -- in particular, interior mutability. Some of our intuitions implicitly rely on other axioms that are incompatible with the ones assumed by Rust: combining them, extending the existing rules in the ways we would suggest, would be unsound. In particular, one should be *very* careful before hiding an `unsafe` block under a macro.

We conjecture that these difficulties are in part caused by the way the Rust language developed over time, with a relatively inexpressive core language, and a lot of the practical expressive power coming from blessed libraries introducing certain lifetime usage patterns. These blessed libraries introduce specialized ownership axioms, justified by a semantic soundness proof rather than by their admissibility in a more expressive inference system.

## Comparison with other approaches

## GCs in Rust — Related works {#sec:rust-GC-preview}

This work can also be seen as safely adding a GC to Rust. As far as
the safe treatment of GCs in Rust is concerned, various prototypes
have inspired this work:

- \citet*{manishearth} has proposed GC values as resources, where 1)
  being a root is tracked as a property of the value using trait-based
  mechanisms, 2) the GC is non-moving (whereas many of our issues
  arise when allocating inside the GC heap can invalidate pointers).
- \citet*{caml-oxide} has proposed a proof of concept emulating the
  callee-roots OCaml FFI discipline with Rust macros. The initial
  version of `ocaml-interop` extended this proof of concept into a
  library. The macro-heavy approach made its soundness more difficult
  to reason about, and less idiomatic for the user, than the current
  version using Boxroot.
- \citet*{josephine} has proposed the safe manipulation of a
  JavaScript GC in Rust with a caller-roots protocol. The difficulty
  of applying this work directly made us notice the importance of the
  conceptual distinction between callee-roots and caller-roots. In
  that work, roots are registered with stack-allocated linked lists.
  The specific rooting mechanism is at the moment of writing this
  paper subject to a longstanding soundness issue. Boxroot brings both
  more safety and more expressivity to this approach.

### Callee-roots: OCaml local roots in Rust ? {#sec:callee-roots-rust}

Previously, \citet*{caml-oxide} has demonstrated a safe lifetime
discipline for OCaml's local roots. We worked on improving this
approach by making it less macro-heavy and more idiomatic to Rust; our
takeaway is that it can be done, but it is hard and the result is
unpleasant to use.

One has to model the fact that at the beginning of the function one
can access either the GC (e.g. allocate), or access the `value`
arguments, but not both at the same time. This involves passing to
every function a capability which is built from several ingredients:

- a dependent ownership type (`OwningHandle` from the crate
  `owning_ref`, a sort of linear $\Sigma$-type) to express that the
  permission to access the values is dependent on the permission to
  access the GC: one can recover a capability to access the GC only by
  relinquishing access to the values (in other words the user is
  forced to root the values if they want to keep using them after
  recovering the GC capability);
- a `LCell` from the crate `qcell`, a container that allows to track a
  permission to access values using a capability separate from the
  values themselves, with a phantom type.

It requires a lot of boilerplate when calling and entering functions,
which is mostly erased at compilation. But it lets us use the Rust
type system to prevent making mistakes.

With Boxroot we have found a simpler solution to the problem of
passing the GC capability along with value, which does not need such
elaborate concepts. But this is nevertheless interesting since it
makes it possible to have a zero-overhead safety layer over the
existing OCaml runtime C API, which is to remain callee-roots.

### Bronze: GC types as Copy types, with stack scanning {#sec:santa}

A different and natural idea is that GC-allocated values should be
manipulated in Rust as plain copiable values, at least as far as
variables <!-- (values placed on the stack at safe points) --> are
concerned. In theory this can be achieved either with conservative
scanning of the stack, or by modifying the Rust compiler so that it
would produce the information the GC needs to find GC roots in the
stack. \citet*{CoblenzMazurekHicks2022} proposes an evaluation for such
a language design, which would seem to remove the need for our GC
roots as an abstraction. In addition, Rust's LLVM back-end is in
theory able to emit precise stack maps, which would make the scanning
of roots on the stack more efficient than Boxroot for this usage (we
are not aware yet of a modified Rust compiler which is able to emit
such stack maps).

<!-- TODO: Citer travail sur v8:
https://docs.google.com/document/d/1uRGYQM76vk1fc_aDqDH3pm2qhaJtnK2oyzeVng4cS6I/edit#heading=h.1wje47kiklhn
-->

However, the two approaches are complementary:

- Boxroot also solves the questions of how to store a GC-allocated
  value inside an arbitrary foreign value (not owned by the OCaml GC),
  and the two mechanisms to register roots can be combined;

- the approach in \citet*{CoblenzMazurekHicks2022} requires a modified
  compiler, whereas Boxroot is more readily applicable to arbitrary
  combinations of languages without having to build support into the
  compiler.

<!-- Gabriel: j'ai l'intuition que l'approche "foreign roots" est plus
modulaire; l'approche Bronze marche quand il y a un seul GC qui gère
tout le système, mais en pratique pour un programme hybride on doit
souvent gérer le fait que plusieurs gestionnaires de mémoire différent
travaillent ensemble, et les "foreign roots" sont nécessaires pour
composer ces systèmes entre eux. C'est lié à ce qui est écrit dans le
second point, mais un peu différent (l'argument clé ici c'est: un seul
gestionnaire mémoire ou possibilité d'en composer plusieurs.) -->

# Related works

## Rooting in other tracing GCs: a survey

TODO

### Static analyses for safe FFIs {#sec:static-analyses}

Previous works approached the safety problem of the GC discipline in
foreign code from the angle of static analysis \citep*{Furr2005FFI,
KondohOnodera2008JNI}. \citet*{Furr2005FFI} proposed and implemented a
static analysis for C, building on the CIL tool
\citep*{NeculaEtAL2002CIL}, to verify the correct usage of the OCaml
FFI. Notably, it used a type-and-effect system to track where GC can
happen. \citet*{KondohOnodera2008JNI} implemented typestate analyses.

\citet*{Furr2005FFI} have found that _"programmers do misuse these
interfaces, and_ [that their] _implementation has found several bugs and
questionable coding practices in_ [their] _benchmarks"_. Similarly,
\citet*{KondohOnodera2008JNI} _"showed that_ [their] _tool could find
many errors in real applications using JNI"_.

Despite these previous works, no tool for finding bugs in this area
currently exist for the OCaml-C interface; in addition we are not
aware in general of a tool ensuring that a discipline is entirely
respected (let alone with a relaxed discipline that seeks to avoid
rooting for performance). <!-- TODO: Can melocoton be cited here? -->

#### The Camlroot experiment

In the context of the OCaml-C FFI, \citet*{camlroot} was a proposal
born out of the experience of writing OCaml bindings to the Qt
library. It also previously advocated the passing of roots instead of
values, allocated in regions, with one argument being that it is more
amenable to dynamic checks than OCaml's mechanisms.

### TODO

... the rest

# Conclusion

<!-- TODO: acknowledge the nice suggestion of Alistair. -->

<!-- `\cite{*}\bibliography{boxroot}`{=latex} -->
`\bibliography{boxroot}`{=latex}
