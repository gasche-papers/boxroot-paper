/* Inlined parts of the code. See it on godbolt. We assume it is based
   on Caml_state rather than a dedicated TLS slot (enables optims from
   Rust if we could pass the domain state as argument). */

#include <stddef.h>
#include <stdbool.h>

#define BXR_LIKELY(a) __builtin_expect(!!(a),1)
#define BXR_UNLIKELY(a) __builtin_expect(!!(a),0)

typedef struct bxr_private* boxroot;

typedef unsigned long long value;

typedef union bxr_slot *bxr_slot_ref;

typedef union bxr_slot {
  bxr_slot_ref as_slot_ref;
  value as_value;
} bxr_slot;

typedef struct bxr_free_list {
  bxr_slot_ref next;
  /* length of the list */
  int alloc_count;
  int domain_id;
  /* kept in sync with its location in the pool rings. */
  int classe;
} bxr_free_list;

typedef struct {
  int domain_id;
  bxr_free_list *fl;
} domain_state_t;

static _Thread_local domain_state_t *caml_state_opt;
void set_domain_state (domain_state_t *dom_st) {
    caml_state_opt = dom_st;
}

boxroot bxr_create_slow(value v);

boxroot boxroot_create_with_domst(domain_state_t *dom_st, value init)
{
  bxr_free_list *fl = dom_st->fl;
  bxr_slot_ref new_root = fl->next;
  if (BXR_LIKELY(new_root != (bxr_slot_ref)fl)) {
    fl->next = new_root;
    fl->alloc_count++;
    new_root->as_value = init;
    return (boxroot)new_root;
  }
  return bxr_create_slow(init);
}

boxroot boxroot_create(value init)
{
  domain_state_t *dom_st = caml_state_opt;
  if (BXR_LIKELY(dom_st != NULL)) return boxroot_create_with_domst(dom_st, init);
  return bxr_create_slow(init);
}

/* Log of the size of the pools (12 = 4KB, an OS page).
   Recommended: 14. */
#define BXR_POOL_LOG_SIZE 14
#define BXR_POOL_SIZE ((size_t)1 << BXR_POOL_LOG_SIZE)
/* Every DEALLOC_THRESHOLD deallocations, make a pool available for
   allocation or demotion into a young pool, or reclassify it as an
   empty pool if empty. Change this with benchmarks in hand. Must be a
   power of 2. */
#define BXR_DEALLOC_THRESHOLD ((int)BXR_POOL_SIZE / 2)

#define Bxr_get_pool_header(s)                                      \
  ((bxr_free_list *)((value)(s) & ~((value)BXR_POOL_SIZE - 1)))

bool bxr_free_slot(boxroot root, bxr_free_list *fl)
{
  /* We have the lock of the domain that owns the pool. */
  bxr_slot_ref s = (bxr_slot_ref)root;
  bxr_slot_ref next = fl->next;
  s->as_slot_ref = next;
  fl->next = s;
  int alloc_count = --fl->alloc_count;
  return (alloc_count & (BXR_DEALLOC_THRESHOLD - 1)) == 0;
}

void bxr_delete_debug(boxroot root);
void bxr_delete_slow(boxroot root, bxr_free_list *fl);

void boxroot_delete_local(boxroot root)
{
  bxr_free_list *fl = Bxr_get_pool_header(root);
  if (BXR_UNLIKELY(bxr_free_slot(root, fl))) bxr_delete_slow(root, fl);
}

void boxroot_delete(boxroot root)
{
  domain_state_t *dom_st = caml_state_opt;
  bxr_free_list *fl = Bxr_get_pool_header(root);
  bool remote = BXR_UNLIKELY(dom_st == NULL) || BXR_UNLIKELY(fl->domain_id != dom_st->domain_id);
  if (BXR_UNLIKELY(remote)) bxr_delete_slow(root, fl);
  else boxroot_delete_local(root);
}

void bxr_modify_debug(boxroot *rootp);
bool bxr_modify_slow(boxroot *rootp, value new_value);

/* Assumes domain lock is held */
bool boxroot_modify_with_dom_lock(boxroot *rootp, value new_value)
{
  bxr_slot_ref s = (bxr_slot_ref)*rootp;
  if (BXR_LIKELY(Bxr_get_pool_header(s)->classe == 0)) {
    s->as_value = new_value;
    return true;
  }
  /* We must reallocate, but this reallocation happens at most once between
     two minor collections. */
  return bxr_modify_slow(rootp, new_value);
}

bool boxroot_modify(boxroot *rootp, value new_value)
{
  if (BXR_LIKELY(caml_state_opt != 0)) {
    return boxroot_modify_with_dom_lock(rootp, new_value);
  }
  /* The domain lock is not held; fused with the previous call. */
  return bxr_modify_slow(rootp, new_value);
}
