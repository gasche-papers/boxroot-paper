\appendix

# Background: manipulating managed values in FFIs {#sec:background-roots}

In this section, we review in detail the techniques used in current
programming languages, ranging from almost automatic to fully manual.
<!--This qualifies both the (positive) work done to declare to the GC
which locations should be considered root for the GC (for instance by
calling appropriate functions of the FFI), and the (negative) work
done to ensure that no root has been forgotten, for instance by
ensuring that an additional discipline has been followed.--> We review
the state of art in the OCaml and Java FFIs, as well as various
attempts at making such FFIs safe. We also mention conservative stack
scanning and other invasive techniques. <!-- TODO: expand with other
languages. Mention conservative stack scanning (Oilpan). -->

## Local roots without indirection using a shadow stack (OCaml) {#sec:local-roots}

_Local roots_ in the OCaml FFI are one way to record GC roots for
OCaml in C. They are implemented with a shadow stack that the
programmer has to manage themselves, using convenience macros that
register local variables and deregister them at return points. As a
typical example:

```c
    value local_fixpoint(value f, value x)
    {
        CAMLparam2(f, x);
        CAMLlocal1(y);
        y = caml_callback(f,x);
        if (compare_val(x,y)) {
            CAMLreturn(y);
        } else {
            CAMLreturn(local_fixpoint(f, y));
        }
    }
```

\noindent In words, the `CAMLparam2(f, x)` macro will register `f` and
`x` as GC roots by storing their address in a linked list on the
stack, which gets visited by the GC when it is looking for roots.
`CAMLlocal1` declares a local variable of type `value` and stores its
address in the same way. When the GC scans the list, `f`, `x` and `y`
get updated if the OCaml blocks they point to are moved by the
GC---for instance here during `caml_callback(f,x)`, which evaluates
the application` f x `<!-- force large spaces -->in OCaml.
Eventually, `CAMLreturn` un-registers those three local roots, by
unlinking the current frame from the linked list.

The `value` type in C represents OCaml values: either an immediate—a
tagged integer—or a pointer into the heap. A `value` in C may or may
not be registered as a root. It is up to the user to ensure that all
values kept alive across a collection point are registered as a root.
For this, the OCaml-C FFI promotes a callee-roots protocol: the
function `local_fixpoint` is in charge of registering its value
parameters with the GC. This is a natural approach since FFI functions
receive `value`s directly, and not some indirection, and
correspondingly manipulating values has no overhead.

\subsubsection*{Limitations}

The local roots macros `CAMLparam`/`CAMLlocal` permit the declaration
of roots with lexically-scoped lifetimes. They cannot be used to store
values in a foreign data structure, or to communicate values between
threads.

The OCaml FFI documentation advocates to follow the above discipline
systematically<!--, which is assumed to be easy for a C programmer to
do consistently and to review -->. However, users often avoid to
declare some variables as root, for performance or for convenience, by
tracking carefully which locations in the code might trigger garbage
collection, and being careful that all variables that live through a
GC are still registered as a root. Whether following the documentation
closely or following a relaxed discipline, this FFI turns out to be
error-prone (Section @sec:static-analyses).

With Rust in mind, a further issue with local roots is that the
`value` parameters registered as roots are not movable: only their
current addresses on the C stack are registered with the GC. If one
calls another FFI function with `f` or `x` as an argument, then that
function will get these values at a different address and needs to
re-register them. There is no way to return values that remain
registered. In particular, encoding this approach in terms of Rust
static discipline gives non-idiomatic Rust APIs that are complex and
unpleasant, as we will see (Section @sec:callee-roots-rust).

## Global roots without indirection (OCaml)

_Global roots_ are another tool offered by the OCaml FFI, used to
register statically- or dynamically-allocated memory as roots for the
GC. This encompasses the storing of values in foreign data structures
and the communication of values between threads.

(Generational) global roots use the following API for root
registration and deregistration:

```c
    void caml_register_generational_global_root(value *);
    void caml_remove_generational_global_root(value *);
    void caml_modify_generational_global_root(value *r, value newval);
```

\noindent With this interface, an arbitrary `value *` address can be
registered as a root for the OCaml GC. These global roots are said
_generational_ because they discriminate between young and old values
(i.e. whether they are allocated in OCaml's minor heap) at
registration time. By keeping track of which roots point to young
values, it is possible to avoid scanning most of the roots at the
start of the minor GC.

\subsubsection*{Example}

One use-case for global roots, which local roots do not cover, is if one
needs to share a value between threads from C.

```c
    void *create_root(value v)
    {
      value *root = malloc(sizeof(value));
      *root = v;
      caml_register_generational_global_root(root);
      return (void*)root;
    }

    value consume_root(void *r)
    {
      value *root = (value *)r;
      value v = *root;
      caml_remove_generational_global_root(root);
      free(root);
      return v;
    }
```

\noindent In words, `create_root` and `consume_root` use the global
roots API is in conjunction with `malloc`/`free`. These functions can
be used to communicate an OCaml function value `f` between a spawning
thread and its spawned thread as follows (ommitting irrelevant
details):

```c
    void thread_func(void *root)
    {
      caml_acquire_runtime_system();
      value f = consume_root(root);
      caml_callback(f, Val_unit);
      caml_release_runtime_system();
    }

    value spawn_thread(value f)
    {
      void *root = create_root(f);
      thread_create(thread_func, root);
      return Val_unit;
    }
```

\noindent In words, the spawning thread creates a root containing the
function, and passes its ownership to the spawned thread, which
consumes it. This garantees that the functional value is correctly
seen by the GC between the moment the thread is spawned and the moment
it acquires the runtime system.

\subsubsection*{Limitations}

The requirement of taking in charge arbitrary addresses is expensive.
Indeed, the runtime has to store a set of arbitrary addresses with
fast traversal (for GC root scanning) and also fast access to a given
element (for modification or removal). This is implemented in OCaml
with global skip-lists. Registering and removing a global root
therefore has a logarithmic time complexity in the number of roots.

As with local roots, values registered using this interface are not
movable on their own: passing a `value` results in a value with a
different address. It is a common usage pattern^[As revealed by
looking at occurrences of global roots registration functions in the
public OPAM repository, <https://opam.ocaml.org/>.] to encapsulate the
root as a movable resource on top of this interface by `malloc`ing a
`value`-sized block as in the previous example, which can then be
passed around or stored in a foreign data structure.

## Opaque pointers: the JNI

The FFI for Java, the _Java Native Interface (JNI)_
\citep*{Liang1999JNI} offers _local_ and _global references_ to Java
objects. These references are opaque indirections to Java objects: the
foreign code must never inspect the values directly, but only via
provided JNI functions. One major motivation for this constraint is
guaranteeing independence from the representation of objects, and
therefore abstraction from particular Java implementations.

This FFI represents objects via an indirection into tables which the
GC knows about, and which it can access whenever necessary. While a
description of this overall idea appears in the GC Handbook
\citep*{JonesHoskingMoss2011GCHandbook}, implementation strategies
were previously undocumented in the academic literature.^[It is
possible that this question has not been recognized as a worthwhile
topic in memory management; our own motivation in studying expressive
rooting mechanisms came initially from a different angle (Section
@sec:linear-allocation).] We document here the implementation
techniques used in the Hotspot/OpenJDK implementation of the JNI.^[As
of OpenJDK JDK 20.]

### Local references using an arena (JNI)

_Local references_ are created automatically by JNI functions and are
automatically released when the foreign function returns. Their
management is therefore automatic for the user of the FFI. A local
reference deregistration method `DeleteLocalRef` of `JNIEnv` is also
provided for the manual release of local references, because it is
nevertheless necessary to delete them early in some situtations.

The following example illustrates a situation that could go wrong
without `DeleteLocalRef`, where the number of references would not be
bounded statically without it:^[Borrowed from \citep*{Liang1999JNI}.]

```c++
    for (i = 0; i < len; i++) {
      jstring jstr = (*env)->GetObjectArrayElement(env, arr, i);
      … /* process jstr */
      (*env)->DeleteLocalRef(env, jstr);
    }
```

\noindent In words, an array is iterated, creating a local reference
for each element. The number of local references nevertheless remains
bounded, thanks to the manual deletion of the local reference created
at every iteration.

#### Limitations

Unlike OCaml local roots, JNI local references can be passed and
returned without the need to re-register the values as roots, thanks
to the representation via an indirection. Nevertheless, the user must
be careful not to let local references escape the scope of the foreign
call. In particular, local references cannot be used in a different
thread than the one that has created them.

In addition, the programmer is advised to be mindful of excessive
reference creation, despite the fact that they are eventually released
automatically. The previous example shows a situation where calling
`DeleteLocalRef` manually is useful; there are many such situations
where one must be mindful of excessive reference creation.

#### Implementation

In the Hotspot Java VM, local references are allocated using region
(bump-pointer) allocation, using a thread-local linked list of chunks.
Deallocation of individual local references is handled by a
supplementary free list storing deleted local references. In other
words, it amounts to a _reap_ allocator \citep*{BergerEtAl2002}. And
while the regions are fairly small (sized to hold 32 references to
objects), another kind of object references, reserved for internal
Hotspot VM use, similar in functionality to local references and
implemented using large arenas with checkpoints (nested scopes
deallocated at once), further support the analysis of the problem as
one of region allocation.

<!--
Local references:
https://github.com/openjdk/jdk/blob/270cf67e5ff19b082f710d52831f436dd144d883/src/hotspot/share/runtime/jniHandles.hpp

Internal handles (arena with checkpoints (HandleMarks)):
https://github.com/openjdk/jdk/blob/270cf67e5ff19b082f710d52831f436dd144d883/src/hotspot/share/runtime/handles.hpp
-->

#### The Camlroot experiment

In the context of the OCaml-C FFI, \citet*{camlroot} was a proposal
born out of the experience of writing OCaml bindings to the Qt
library. It also previously advocated the passing of roots instead of
values, allocated in regions, with one argument being that it is more
amenable to dynamic checks than OCaml's mechanisms<!-- an aspect
which we did not investigate, since our goal regarding safety is to
perform static checks using the Rust type system -->.

<!--
  Note: the region/arena support is found within the Cuite project:
    https://github.com/let-def/cuite/blob/master/cpp/cuite_csupport.c
    https://github.com/let-def/cuite/blob/master/cpp/cuite_csupport.h
    https://github.com/let-def/cuite/blob/master/cpp/cuite_gc.h
-->


### Global references using a concurrent allocator (JNI)

A global reference is created from a local reference using the
`JNIEnv` method `NewGlobalRef` and remains valid until it is deleted
with the `JNIEnv` method `DeleteGlobalRef`. Unlike local references,
there is no automatic deletion, and _global references_ can be used
across threads and foreign function calls.

For instance, an object can be stored across invocations as
follows^[Example again inspired from \citep*{Liang1999JNI}.]:

```c++
    void f(JNIEnv *env)
    {
      static jclass str = NULL;
      …
      str = (*env)->NewGlobalRef(env, localStr);
      if (str == NULL) {
        /* allocation failure */
        …
      }
      (*env)->DeleteLocalRef(env, localStr);
      …
    }
```

\noindent In words, a static variable `str` is used to store, across
invocations of a function, a global reference created from a local
one, `localStr`. It would be incorrect to store `localStr` directly in
the static variable. <!--One checks that the allocation of a global
reference has suceeded. Lastly, once a global reference is created,
the local reference can be deleted if necessary. -->

The JNI also supports other flavours of global references such as weak
global references (which do not keep the managed value alive).

#### Implementation

Global references are implemented in the Hotspot VM with a concurrent
bitmap allocator. This allocator is made of a global linked list of
chunks from which references are allocated, each of which tracks its
allocation with a bitmap. The chunks are sized for efficient atomic
modification and efficient retrieval of the first available element
from the bitmap, using bit-scanning processor instructions (that is,
64 objects on 64-bit processors).

Concurrent allocation is handled by taking a mutex on the global data
structure, and by updating the allocation bitmap atomically. The
concurrent deletion of a global reference is lock-free, and uses a CAS
loop to update the allocation bitmap. As noted in the source code
documentation, the deletion of a global reference \emph{“must be
lock-free because it is called in all kinds of contexts where even
quite low ranked locks may be held”}.

<!--
Global references:
-  https://github.com/openjdk/jdk/blob/270cf67e5ff19b082f710d52831f436dd144d883/src/hotspot/share/gc/shared/oopStorage.cpp#L389-L434 bitmap allocator
-->

#### Limitations

Global references, as implemented in the Hotspot VM, improve on
`malloc`ed OCaml global roots by combining two allocations in one.
This is key to lifting the logarithmic complexity associated with
OCaml global roots.

However, global references are likely unable to replace local
references for two reasons. One is the lower cost of creation and
deletion for local roots; the other is the added convenience of the
automatic deletion of the latter in languages such as C and old C++.

#### In this paper

Essentially, with Boxroot we revisit global references in the context
of OCaml, with aims to do away with local references. To this effect,
we investigate a more efficient allocation method drawing from modern
concurrent allocator techniques and GC implementation techniques, and
a more lightweight approach to safety by avoiding to register all the
managed values as a root.

<!-- Notes on GC roots/handles implementations in other runtime system.

## Distinction

- already in the paper: transparent vs. opaque representation of
  managed objects (access the underlying representation directly, or
  only through an "opaque handle" abstraction that offers
  introspection function guaranteeing complete rooting)

- not clearly in the paper: shallow vs. deep FFI.

  Deep FFIs are designed to let foreign code manipulate arbitrary
  language values, with an expressivity comparable to native /
  non-foreign code. In particular, foreign code must interact with the
  garbage collector just like native code.

  In a shallow FFI, only shallow data is exchanged across the
  boundary, that does not contain managed pointers and thus needs not
  be rooted. This is in fact a relatively common patterns in FFIs
  designed to "represent C values in the language directly" --
  Haskell, SML, dotnet -- to binding foreign libraries from the
  language itself, instead of writing foreign code.


## JNI / jni.hpp

JNI Global Refs As smart pointer:

https://github.com/mapbox/jni.hpp/blob/master/include/jni/ownership.hpp
using UniqueGlobalRef = std::unique_ptr<T, Deleter<&JNIEnv::DeleteGlobalRef>>;


## Python

Boost::python & pybind11
"bring your own smart pointer"
I didn't manage to understand much more by reading the code (Guillaume)


## CLR/dotnet runtime

### GC handles in coreclr/gc

The dotnet runtime supports many flavors of handles, including weak
pointers/references and ephemerons. ([Gabriel] I find this unification
of concepts interesting, I wonder if this can result in better code,
and possibly better performance.)

The many types of handles are described clearly in
    https://github.com/dotnet/runtime/blob/8e23fecbd748009d3e9699e69fa0a56a1342493c/src/coreclr/gc/gcinterface.h#L370-L506

- Weak handles: weak pointers/references,
  with two variants depending on whether they survive revival by the finalizer

- Dependent handles: ephemerons, a refinement of weak handles.
- Strong handles: the usual sort of GC handles.

- Pinned handles: handles that pin the underlying object, similar to
  the Chez Scheme implementation strategy. Interesting comment:

   > NOTE:  PINNING AN OBJECT IS EXPENSIVE AS IT PREVENTS THE GC FROM ACHIEVING
   >        OPTIMAL PACKING OF OBJECTS DURING EPHEMERAL COLLECTIONS.  THIS TYPE
   >        OF HANDLE SHOULD BE USED SPARINGLY!

- Refcounted handles: a ref-counted reference, for
  interoperability with ref-counted foreign memory
  (used in both Windows COM and Objective-C). Subtle!

  The handle itself is ref-counted by the foreign mutator, and the GC
  queries the reference count: if it falls down to 0, then the handle
  is treated like a weak handle (it does not keep the GCed object
  alive anymore).

  Question: why is the handle treated as a weak handle in this case,
  instead of simply being deallocated/destroyed? This seems designed
  to support workflows where the handle is also used outside the
  ref-counted mutator (for example, maybe in a system interoperability
  layer between the ref-counting and the GC), and we want to be able
  to dynamically check for validity of the handle at that level?

- Some other handle types that are deprecated or seem unused
  in the runtime itself.

The implementation of handle tables seems more complex and slower than boxroots.
- there are two levels of caches (a "quick cache" and another cache)
  before we hit the actual "table"
- even accessing the cache requires synchronization operation
  (two threads may be racing to access the same cache)
- my *guess* from reading the code is that it is less efficient in
  a multithreaded context because it is not trying to use separate
  pools for different threads, so each access incurs synchronization
  overheads

GC handles in vm/appdomain.cpp
  https://github.com/dotnet/runtime/blob/8e23fecbd748009d3e9699e69fa0a56a1342493c/src/coreclr/vm/appdomain.hpp
  https://github.com/dotnet/runtime/blob/8e23fecbd748009d3e9699e69fa0a56a1342493c/src/coreclr/vm/appdomain.cpp

  These seem to be more specialized sort of handles that are defined
  *on top* of the GC handles mentioned above -- you can easily
  implement your own handles as elements of a GC-allocated array that
  is registered as a strong handle within the runtime.

  Currently I don't understand the rationale for this extra layer of
  abstraction, from a distance I would think that using GC handles
  directly would be just as efficient or more -- the implementations
  here seem non-trivial but still more naive. This may be a case of
  different part of an organization doing redundant work, rather than
  a proper design choice.

  PinnedHandleTable: handles allocated in a "pinned heap"
  ThreadStaticHandleTable: handles allocated in a "large heap"

  PinnedHandleTable:
  - a list of buckets of about 64K handles
  - possibility of 'batch' allocation
    that allocates many handles at once
  - bump-limit allocation strategy,
    with reuse only for requests of size one
    (no free list, so reuse needs some scanning)
  - deallocation writes a 'sentinel object'
  - handle buckets are allocated by the GC
    (tricky reentrancy considerations to allocate a new bucket)
  - expose their own scanning method

  ThreadStaticHandleTable:
  - a "strong handle" to a gc-allocated array
  - each batch request for handles allocates a new array (bucket) of
    the requested size


## GHC runtime

Mostly a shallow FFI, but there are two ways to export Haskell Data to the C side:

- "Pinned objects": non-moving (shallow) objects -- may not contain pointers
- "Stable pointers": GC handles in the usual sense
  but fairly naive implementation (a single big dynamic array with a global lock)
  stable pointers not supposed to be dereferenced from C, only stored in C data structures
  and then passed back to the Haskell world

Overview post:
  https://www.well-typed.com/blog/2018/05/ghc-special-gc-objects/
Haskell doc:
  https://hackage.haskell.org/package/base-4.19.1.0/docs/Foreign-StablePtr.html#t:StablePtr
Implementation:
  https://github.com/ghc/ghc/blob/38a4b6ab7bbd5e650f43541d091a52ed2655aff6/rts/StablePtr.c
  https://github.com/ghc/ghc/blob/38a4b6ab7bbd5e650f43541d091a52ed2655aff6/rts/StablePtr.h

Stable pointers are stored in one big dynamic array (with a free list):
- a lock that protects the table for allocation, resizing, and collection
- dereferences are lock-free, they use release/acquire

Ticket to make it more efficient (generational and per-capability) was
opened 11 years ago and never worked on:
  https://gitlab.haskell.org/ghc/ghc/-/issues/7670

## Chez Scheme

Deep FFI.

The Chez GC can 'immobilize' objects temporarily, and offers a Slock_object primitive for the FFI code to do this.

  https://github.com/cisco/ChezScheme/blob/fc081fc447a786dd53286e5d7314b7217631cb68/c/gcwrapper.c#L298-L321
  https://github.com/cisco/ChezScheme/blob/fc081fc447a786dd53286e5d7314b7217631cb68/c/gc.c#L35-L36

Chez uses a moving collector by default, but moves to mark&sweep for segments that contain immobilized objects.

## Ruby

Runtime ability to 'pin' an object with `gc_pin`:
  https://github.com/ruby/ruby/blob/b4b39a619984bbb3c96406a1eb4dfb477168a1cd/gc.c#L6703

Most of the runtime uses this for objects that are accessible from
C. The idea is that only Ruby-allocated objects will be moved around
by compaction, but this is okay because they probably take most of the space.

Explanation of their "mostly-moving" design.
  https://github.com/ruby/ruby/blob/b4b39a619984bbb3c96406a1eb4dfb477168a1cd/include/ruby/internal/gc.h#L171-L199

By default C extensions are encouraged to manipulate pinned objects
only, but they can also manipulate non-pinned objects by using
`rb_gc_location` which follows moved/forward pointers. (This assumes
that the C FFI can reason about the absence of compaction, but not
about the absence of the moving GC.) This seems to be an internal API
that external users do not know much about.

Questions: How does this work in practice, what guarantee do they
provide on how long previous adresses will remain available as forward
pointers? How can this work with a minor generation that is collected
often and its memory reused?

There are other functions to register-and-pin objects
- `rb_gc_register_adress`: registers in a singly-linked list of globals,
  with linear-lookup deletion
- `rb_gc_register_mark_object`: registers into a linked list of pools

Documentation of pinning for users:
  https://alanwu.space/post/check-compaction/

This is a mess, and there are very few resources online to explain
what is going on (and a lot of the English discussions are naive
or misinformed).


## SML and MLton

SML uses a shallow FFI, we did not find any support for rooting SML values from C.

## Racket

Adding roots is possible (in a large global dynarray), but I haven't found a mechanism to remove those roots.

https://github.com/racket/racket/blob/35f779d15604d8ab458ba0bc49a673947d0d031b/racket/src/bc/gc2/roots.c#L82-L94

## SBCL (Common Lisp)

Shallow FFI. There is an 'immobile space' for values that are never moved, but it is limited and was designed for other use-cases than FFI.

  https://www.sbcl.org/manual/#Calling-Lisp-From-C
  https://github.com/sbcl/sbcl/blob/503d086c13c601257c476bbfa12e4957a2f1f8b3/src/runtime/immobile-space.c
  https://applied-langua.ge/~hayley/swcl-gc.pdf

## V8

Oilpan GC for C++ that allows combined C++/Javascript tracing
  https://v8.dev/blog/high-performance-cpp-gc
  https://docs.google.com/document/d/1y7_0ni0E_kxvrah-QtnreMlzCDKN3QP4BN1Aw7eSLfY/edit
    > conservative stack scanning
    > Keeping objects alive from non-managed memory through Persistent smart pointers which are treated as roots.
    > Designed to run the GC at the toplevel of the event loop most of the time, so that there is no stack to scan (and so we are precise and can do compaction
    > "Referencing Heap Allocated Objects from Outside the Oilpan Heap"
      > "Do NOT use Persistent handles in objects allocated in the oilpan
      > heap. Persistent handles in objects allocated in the oilpan heap
      > can lead to cycles that cause memory leaks. We will add static
      > and dynamic verification that Persistent handles are not
      > allocated in objects in the oilpan heap."

  https://storage.googleapis.com/gweb-research2023-media/pubtools/pdf/eebe9196179ca5f25354ff4c49a0633a7a079c6b.pdf
  https://dl.acm.org/doi/10.1145/3276521
    > Cross-Component Garbage Collection
      > looks at the problem of cross-component cycles
      > Strengthens the 2nd contribution (our own original solution is ownership (RC=1) to avoid cycles and handles placed on the GC heap)
      > Does not describe an implementation technique for handles
    > see also https://dl.acm.org/doi/10.5555/1404014.1404043 ?
    > see also https://dl.acm.org/doi/10.1145/1993478.1993492
      > Handles Revisited: Optimising Performance and Memory Costs in a Real-Time Collector
      > Handles everywhere, even the heap. Goal: real-time GC. Significantly changes the problem (the GC must deallocate handles)
      > Studies efficient implementations. TODO: look at their optimizations.
  https://chromium.googlesource.com/v8/v8.git/+/HEAD/include/cppgc/

look for root-registration structures
  https://chromium.googlesource.com/v8/v8.git/+/HEAD/include/cppgc/heap-handle.h
  https://chromium.googlesource.com/v8/v8.git/+/HEAD/include/cppgc/persistent.h
  https://chromium.googlesource.com/v8/v8.git/+/HEAD/include/cppgc/internal/persistent-node.h
  > using PersistentNodeSlots = std::array<PersistentNode, 256u>;
  > std::vector<std::unique_ptr<PersistentNodeSlots>> nodes_;
  https://chromium.googlesource.com/v8/v8.git/+/HEAD/src/handles/
  https://chromium.googlesource.com/v8/v8.git/+/HEAD/include/v8-local-handle.h#233
  > There now is Local and Persistent. Local: probably an arena
  > // Handle is an alias for Local for historical reasons.

v8
  > Oilpan: persistent (boxroot-like) and conservative stack scanning
  > Now v8: Local (arena, stackable, without freelist) & Persistent. Still conseravtive stack scanning & direct handles in option
    > https://chromium.googlesource.com/v8/v8/+/master/src/flags/flag-definitions.h#492
    > Compile-time choice: either indirect local handles, or direct ones which require conservative stack scanning
  > https://chromium.googlesource.com/v8/v8.git/+/HEAD/include/cppgc/internal/persistent-node.h#167
    > Uses a lock for the cross-thread variant. A single-thread variant exists.
  > https://chromium.googlesource.com/v8/v8.git/+/HEAD/include/v8-persistent-handle.h#455
    > The general api relies on a call to DisposeGlobal. It is defined
      in src/api.cc though strangely the github interface does not let
      you find it through search.
    > Starts here: https://chromium.googlesource.com/v8/v8.git/+/HEAD/include/v8-persistent-handle.h#455
      > Blocks of 256 objects, 32B per handle (!); a global freelist. No parallelism.
      > Generational optim: generation check at allocation & stored in a young list
  > Tracing trait mechanism for finding roots deep in structures


## SpiderMonkey (Firefox)

### In the "GC rooting guide"

https://github.com/mozilla-spidermonkey/spidermonkey-embedding-examples/blob/esr78/docs/GC%20Rooting%20Guide.md

- Js::Rooted<T> for GC handles stored on the stack,
  which follow a LIFO discipline
- Js::Handle<T>, which is a reference to a Js::Rooted<T>,
  for function arguments (a parent caller rooted the object)
- Js::MutableHandle<T> for out-parameters
- return values can be unrooted, but they must be rerooted by the caller

- a Js::CustomAutoRooter class for values implementing their own tracing logic,
  with the common case Js::AutoVector<T> predefined

- Js::Heap<T> for GC handles stored on the heap. Note that Js::Heap<T>
  values are not "rooted", they must be traced -- the only service they
  provide is to follow the pointed object when it is moved.

Js::Rooted<T> objects (and the corresponding Js::Handle) must have
a LIFO lifetime, so it is unsafe to:
- store them on the heap
- return them from a function

### In the source

- general comment:
  https://searchfox.org/mozilla-central/rev/8ec3cc0472ad4f51b254728d024b696eaba82ba0/js/public/RootingAPI.h#34
- detailed documentation on Heap<T>:
  https://searchfox.org/mozilla-central/rev/8ec3cc0472ad4f51b254728d024b696eaba82ba0/js/public/RootingAPI.h#273
  (and TenuredHeap<T>, an optimized version for handles to objects that are only in the old heap
   https://searchfox.org/mozilla-central/rev/8ec3cc0472ad4f51b254728d024b696eaba82ba0/js/public/RootingAPI.h#446
  )
  This type is in charge of placing read and write barriers on access to
  its subvalue from FFI code, but it does not need to register itself
  with the GC to keep the value alive. In other words, it is not
  a (strong) root.

- implementation of Rooted<T>
  https://searchfox.org/mozilla-central/rev/8ec3cc0472ad4f51b254728d024b696eaba82ba0/js/public/RootingAPI.h#1138
  https://searchfox.org/mozilla-central/rev/8ec3cc0472ad4f51b254728d024b696eaba82ba0/js/public/RootingAPI.h#910

  The inherited StackRootedBase ensures that each Rooted<T> object has
  a field available to store an inline singly-linked lists of
  registered values. Tracing is done by traversing this list
  (TraceExactStackRootList in RootMarking.cpp). I suppose that
  un-registration is simply done by resetting the list pointer to its
  initial value, giving an "arena" behavior to these Rooted<T> in
  practice, but I haven't located the code to do this for Rooted<T> --
  when does the unregistration happen?

## JavaScriptCore

- a conservative root scanning approach
  https://github.com/WebKit/WebKit/blob/1c70b1ecc16aa38c8042c5d01f03f3ee92f2b203/Source/JavaScriptCore/heap/ConservativeRoots.cpp
- but also GC handles
  https://github.com/WebKit/WebKit/blob/1c70b1ecc16aa38c8042c5d01f03f3ee92f2b203/Source/JavaScriptCore/heap/Strong.h
- ... and something else?

Strong handles are implemented in as slots in blocks of size 4Kio structured in a doubly-linked list.
There is a global free list for all blocks.

A PR https://github.com/WebKit/WebKit/pull/13898 to avoid using
Strong<T> in some part of the runtime and instead hardcoding a list of
strong roots in custom traversal code; the PR still uses the simpler
abstraction WriterBarrier<T> to still use write
barriers. (This resemble Heap<T> in the Mozilla world.)

-->

## Safe manipulation of managed values

### Static analyses for safe FFIs {#sec:static-analyses}

Previous works approached the safety problem of the GC discipline in
foreign code from the angle of static analysis \citep*{Furr2005FFI,
KondohOnodera2008JNI}. \citet*{Furr2005FFI} proposed and implemented a
static analysis for C, building on the CIL tool
\citep*{NeculaEtAL2002CIL}, to verify the correct usage of the OCaml
FFI. Notably, it used a type-and-effect system to track where GC can
happen. \citet*{KondohOnodera2008JNI} implemented typestate analyses.

\citet*{Furr2005FFI} have found that _"programmers do misuse these
interfaces, and_ [that their] _implementation has found several bugs and
questionable coding practices in_ [their] _benchmarks"_. Similarly,
\citet*{KondohOnodera2008JNI} _"showed that_ [their] _tool could find
many errors in real applications using JNI"_.

Despite these previous works, no tool for finding bugs in this area
currently exist for the OCaml-C interface; in addition we are not
aware in general of a tool ensuring that a discipline is entirely
respected (let alone with a relaxed discipline that seeks to avoid
rooting for performance). <!-- TODO: Can melocoton be cited here? -->

### Managed values in Rust {#sec:GC-in-Rust}

Various experiments previously set out to design safe GCs interfaces
in Rust \citep*{manishearth, josephine, caml-oxide}. They tried to
leverage the enforcement of ownership and lifetimes by the compiler to
implement the rules for living in harmony with the GC, not unlike the
the uses of type-and-effect systems and a typestate analyses in the
previous works. We will recall the general ideas behind these
approaches in Section @sec:rust-GC-preview.


#### In this paper

The present work contributes to this line of research, which seeks to
leverage the Rust type system in the best way possible (Section
@sec:boxroot-rust). This approach has several virtues:

- Unlike the static analyses mentioned above, this approach does not
  require the development and maintainance of external tooling. We
  expect our contribution to survive in the long term as a means to
  write safe FFI code for OCaml in Rust<!-- —by the mere
  backwards-compatiblity guarantees offered by the Rust language -->.
- Evidence is hard to come by regarding claims of practicality<!-- of
  programming especially under the constraints a particular static
  analysis (we have mentioned for instance the tension between
  strictly following the rules and desire for performance in the case
  of the OCaml FFI) -->. By reducing the problem to one the Rust
  language is designed to solve, and by fitting Rust idioms and API
  practices, we benefit from the experience accumulated by Rust
  programmers. (As a matter of fact, this pushes Rust to its limits,
  as we run into limitations of the language, as explained in Section
  @sec:limitations.)
- Beyond Rust, the approach also has a value as a proof of concept and
  as a potential source of inspiration for other languages including
  C—by showing that the problem is a particular case of
  resource-safety in these languages.

# Boxroot: fast movable GC roots, for a better OCaml FFI

Compared to previously-mentioned FFIs, our API for root-registration
combines the indirection of JNI references, and the transparency in
value representation of the OCaml FFI.

Boxed roots are created and deleted with functions `boxroot_create`
and `boxroot_delete`:

```c
    boxroot boxroot_create(value);
    void boxroot_delete(boxroot);
```

\noindent These roots can be accessed with `boxroot_get` and
`boxroot_get_ref`:

```c
    inline value boxroot_get(boxroot r) { return *((value *) r); };
    inline value const * boxroot_get_ref(boxroot r) { return (value *)r; };
```

\noindent With this API, the contents of a boxed root can be _borrowed_,
allowing direct access to the value. In particular, when no rooting is
necessary (in parts of code where no allocation happens, typically),
plain values can still be passed around, just like in expert relaxed
OCaml FFI discipline.

The second form of borrowing offers a pointer to the contents of the
boxed root. It expands the relaxed discipline to situations where some
allocations still happen (in which case the contents of the pointer is
kept alive and gets updated to its new value if necessary). But it is
evidently subject to lifetime discipline.

Two distinct factors explain the higher performance that we expect
over OCaml roots, and over JNI references:

1. By enjoying a more expressive and relaxed rooting discipline,
   derived from an understanding of GC roots under the lens of
   resource management, programs need to allocate fewer roots. (And,
   by leveraging a type system enforcing a lifetime discipline such as
   Rust's, later in Section @sec:rust-valueref, this can be done all
   the while increasing the safety of the FFI compared to C and C++.)

2. By understanding roots implementation as an allocation problem, we
   can leverage the accumulated knowledge of modern concurrent memory
   allocators.

In this paper, we focus on an API similar to global references.

We also implemented region-allocated local references for comparison
purposes in our benchmarks. By the combination of allocating fewer
roots and doing so more efficiently, we expect that the returns in
terms of performance of local roots over global roots are diminishing,
letting us propose Boxroot as the go-to rooting mechanism for the
purpose of the OCaml-Rust FFI.^[We believe that local references will
remain worthwhile in languages with limited support for resource
management such as C, due to the convenience of batch deallocation
provided by regions.]

<!-- TODO: Guillaume stopped here. -->

## Lesson 1: managed values as resources {#sec:roots-as-resources}

<!-- TODO -->

We see the creation of roots manipulable from foreign code as
a resource allocation problem. Instead of being implicit in the FFI
API, roots become resources that our API can create, access, update
and delete. Like generic heap-allocated memory in our non-managed
language, our roots can be moved (their ownership transferred) or
borrowed (received without the responsibility to dispose of them), and
stored in data structures obeying ownership semantics themselves.

To enforce the resource-management discipline of roots (prompt release
and correct use), we can treat individual roots as _smart pointers_
(resource containers), obeying similar rules as (for instance) the
reference-counting pointers from C++ and Rust.<!--^[This perspective comes
naturally if one sees reference-counting smart pointers as
implementing a form of garbage collection in C++ and Rust; this also
brings to mind \citet*{Bacon2004}.]--> With this approach, non-managed code
dealing with managed values will feel idiomatic in languages offering
resource-management features, and will naturally integrate in existing
static resource-safety techniques.

Our C example may look as follows using boxroot:
```c
boxroot boxroot_fixpoint_rooted(value const *f, boxroot x)
{
  boxroot y = boxroot_create(caml_callback(*f, boxroot_get(x)));
  int cmp = compare_refs(boxroot_get_ref(x), boxroot_get_ref(y));
  boxroot_delete(x);
  if (cmp)
    return y;
  else
    return boxroot_fixpoint_rooted(f, y);
}
```

\noindent This function follows the caller-roots protocol, so it takes
two roots as argument. In this example `f` is borrowed, whereas `x` is
taken ownership of. Some code transformation is necessary to insert a
call to `boxroot_delete` to release `x` in a timely fashion (in the
general case this can require a resource-management style that uses
forward `goto`s in lieu of exit points). This is not much more
convenient, in C, as the callee-roots version (but convenience in C is
not our focus).

In C++ or Rust for instance, the `boxroot_delete(x)` call would be
implicitly inserted, bringing convenience and extra safety. For
example, a Rust version could be written as follows using the package
[ocaml-interop](https://lib.rs/crates/ocaml-interop) on top of
Boxroot:

```rust
fn fixpoint<T>(gc: &mut OCamlRuntime,
               f: OCamlRef<OCamlFn1<OCaml<T>, OCaml<T>>,
               x: BoxRoot<T>
) -> BoxRoot<T> {
    let y: BoxRoot<T> = BoxRoot::new(&*f.call(gc, &*x));
    if caml_compare_refs(gc, &x, &y) == 0 {
        return y;
    } else {
        return fixpoint(gc, f, y);
    }
}
```

<!-- TODO gm: can code be simplified?

     [Gabriel] I tried this with ocaml-interop, but its handling of
    function types appears to be too primitive to express this -- we
    can only get function closures by asking for registered
    callbacks. The "real" ocaml-interop code I wrote is as follows:

    fn caml_compare_refs<T>(_cr: &OCamlRuntime, _: OCamlRef<T>, _:OCamlRef<T>) -> i64 {return 1;}

    fn fixpoint<T>(gc: &mut OCamlRuntime,
                   f: internal::OCamlClosure,
                   x: BoxRoot<T>
    ) -> BoxRoot<T> {
        let y: BoxRoot<T> = BoxRoot::new(f.call(gc, &*x));
        if caml_compare_refs(gc, &x, &y) == 0 {
            return y;
        } else {
            return fixpoint(gc, f, y);
        }
    }
-->

\noindent In this version, the registration of `y` as a root is
explicit with the `Boxroot::new` constructor, but the deletion of `x`
is made implicit by destructors. The OCaml function `f` expects values
directly, so we dereference the root `x` using `&*x`, borrowing the
dereferenced value for the duration of the call only.

<!-- TODO note: the JNI is value-representation independent; we assume
a value representation. -->

<!-- In this context, opaque roots are _a priori_ simpler to deal with: GC
values are never handled directly, everything is rooted. Thus the GC
can safely run anytime. Yet this is not so simple: 1) in the case of
OCaml, a part of the work must still be done in order to ensure that
the thread holds its _domain lock_, and likewise if one wants to have
some way to control GC safe points; 2) the low-level functions that
access opaque roots must be written at some point, somehow. Thus,
while leveraging the Rust type system can bring performance gains by
avoiding having to root many values, it is work that is useful
regardless. -->


### Lesson 2: synergy between allocator and collector techniques {#sec:efficient-alloc}

Our resource-inspired interface instead leaves the responsibility of
allocating the root to the FFI support library. In other words, our
interface is equivalent to the combination of `malloc` and
registration<!-- TODO: mieux -->.

This design choice provides important performance benefits. Indeed, we
can implement allocation using standard modern concurrent allocation
techniques (e.g. \citealp{Berger2000Hoard}, among others), in the
simple setting of only having one-word allocations---much of the
allocator complexity is removed. Finally, there is a virtuous
interaction between the allocation logic and the root-scanning logic
for a generational collector: the allocator can track young roots to
speedup scanning.

Concretely:

- <!-- TODO: confusing. what did it say before? --> Our API is
  naturally separated, in the sense that boxroots registered by
  different core/threads can be stored in local data structures. We
  thus expect good scaling in concurrent use-cases, better than what
  implementations of the existing global-roots API can provide. <!--
  TODO: restore explantion of why this would be hard for global roots?
  -->

- Even for sequential code, modern allocators provide good cache
  locality, which makes GC scanning faster than with a global roots
  skiplist.<!-- TODO: confusion of 2 concerns? scanning has a good
  cache locality regardless of whether it has a cache-local structure.
  Modern allocators strive for good cache-locality _for the mutator_
  -->

<!--The previous version of the paragraph above was as follows:
This interface has other limitations:

- In OCaml 4, it is necessary to hold the OCaml master lock while
  deregistering a root. This is unsuitable for situations where a root
  is stored in a foreign structure, in a thread that does not always
  hold the master lock. Indeed, the program should always be able to
  clean-up its resources, for instance, during unwinding following a
  panic (exception) in Rust. Obviating other obstacles, acquiring
  OCaml's runtime lock can take up to 50ms, which is not contemplable
  in a destructor.
- In multicore OCaml, the global structure is protected by a mutex.
  This choice supports deallocating a root in a thread that does not
  belong to the domain in which it has been allocated (for instance
  for sending or sharing OCaml values between domains). But it also
  makes it more expensive. It is unclear that there is a simple way to
  make it efficient while still supporting the sending and the sharing
  of roots between threads (and avoiding the previous issue).
  \footnote{One advantage is its simplicity of implementation, but we
  do not know whether there is a simple solution to keep this API and
  have an implementation that scales: per-domain skiplists would lose
  this property and suffer from the previous limitation, while
  lock-free skiplists would impose an overhead on everyone but the
  interaction with the GC during scanning would be delicate.}
- In OCaml's current implementation of global roots, scanning the
  roots involves the traversal of a linked list. While this is optimal
  in terms of complexity, this is more costly than scanning local
  roots, which have a better cache locality.
-->



# Boxroot

## The Boxroot interface

<!-- TODO redite -->
The C interface of our Boxroot library is as follows:

```c
    boxroot boxroot_create(value);
```

`boxroot_create(v)` allocates a new root initialised to the value `v`.
This value will be considered as a root by the OCaml GC as long as the
boxroot lives or until it is modified. The OCaml domain lock must be
held before calling `boxroot_create`.

```c
    inline value boxroot_get(boxroot r) { return *((value *) r); };
    inline value const * boxroot_get_ref(boxroot r) { return (value *)r; };
```

`boxroot_get(r)` returns the contained value, subject to the usual
discipline for non-rooted values. `boxroot_get_ref(r)` returns a
pointer to a memory cell containing the value kept alive by `r`, that
gets updated whenever its block is moved by the OCaml GC. The pointer
becomes invalid after any call to `boxroot_delete(r)` or
`boxroot_modify(&r,v)`. The OCaml domain lock must be held before
calling `boxroot_get` or before deferencing the result of
`boxroot_get_ref`.

```c
    void boxroot_delete(boxroot);
```

`boxroot_delete(r)` deallocates the root `r`. The value is no longer
considered as a root by the OCaml GC afterwards. One does not need to
hold the OCaml domain lock before calling `boxroot_delete`.

```c
    void boxroot_modify(boxroot *, value);
```

`boxroot_modify(&r,v)` changes the value kept alive by the root `r` to
`v`. The OCaml domain lock must be held before calling
`boxroot_modify`. It is essentially equivalent to the following:

```c
    boxroot_delete(r);
    r = boxroot_create(v);
```

\noindent However, `boxroot_modify` is more efficient: it avoids
reallocating the boxroot if possible. The reallocation, if needed,
occurs at most once between two minor collections.

### Alternative: a doubly-linked list {#sec:dll}

The reader might wonder what is the simplest reasonable implementation
of the boxroot interface. It is possible to give a simple
implementation with a doubly-linked list (or two, with an obvious
generational optimisation to have reasonable performance). This can be
seen as our own boxroot implementation, essentially, but for memory
pools of size 1. We have implemented a naive version of it, without
thread-safety, for comparison purposes. (This control was suggested to
us by Frédéric Bour.)

## Implementation of Boxroot {#sec:implem}

Boxroot is a custom multi-threaded allocator that manages fairly
standard freelist-based local memory pools (see
\citealp{Berger2000Hoard}, among others). Our allocator manages a
unique size, the size of OCaml values (64 bits on 64-bit
platforms).\footnote{With additional effort, it is possible to manage
arbitrary unboxed memory layouts, that mix GCed values treated as
roots and arbitrary data.} But we arrange to scan these pools
efficiently.

In the implementation benchmarked in this paper, the pool metadata
(the head of its free list, the number of allocated elements, pointers
to the next and previous pools, etc.) is placed at the start of the
pool, which itself is suitably aligned, in such a way that from the
elements of the pool, the boxroots, one can access efficiently from
the most significant bits of their elements (following a common
technique).

Now, thanks to the alignment, since elements of the freelist are
guaranteed to point only inside the memory pool, and non-immediate
OCaml values are guaranteed to point only outside of the memory pool,
we can identify allocated slots as follows:
$$\mathtt{allocated}(\mathtt{slot},\mathtt{pool})
\mathrel{\overset{\textrm{def}}{=}}
\texttt{(\ensuremath{\mathtt{pool}} != (\ensuremath{\mathtt{slot}} \&
\textasciitilde(1<{}<\ensuremath{N} - 2)))}$$

\noindent $N$ is a parameter determining the size of the pools, $2^N$.
(In our case, $2^N=16\,\textrm{KB}$, corresponding to 2032 roots per
pool after subtracting the size of the pool metadata.) The bitmask is
chosen to preserve the least significant bit, such that immediate
OCaml values (those with their least significant bit set) are
correctly classified.

### Generational optimisations

We take advantage of GC generations in several ways.

The memory pools are managed in one of several rings, depending on
their *class*. The class distinguishes pools according to OCaml
generations, as well as pools that are free (which need not be
scanned). A pool is *young* if it is allowed to contain pointers to
the minor heap. During minor collection, we only need to scan young
pools. At the end of the minor collection, the young pools, now
guaranteed to no longer point to any young value, are promoted into
the ring of *old* pools.

When allocating a new root, we unconditionally allocate it in a young
pool, without testing at allocation-time whether the initial value is
young or old. Indeed, we have found that this test is better done
during scanning, for several reasons:

- in some situations, there may be many more roots allocated than
living through collections (for instance in the use-case of local
roots);
- it is possible to discriminate between old and young values more
efficiently in a tight loop, for instance when scanning roots for the
minor GC;
- we noticed the benefits of inlining the fast paths of all the
operations (create, get, get_ref, delete and modify). So we must be
careful to avoid negative real-world effects of inlining (which would
be hard to notice in our current benchmarks), such as increased code
size and introducing branches that are not statically predictible.

\noindent Inlining is interesting in use-cases with short-lived roots,
where cache misses are not expected to be the dominant cost, such as
when used in replacement of local roots (Section @sec:local-bench).

When the current pool is full, we check if a young pool is available.
If not, then we demote an old pool into a young pool, but only one
which is less than half-full. Otherwise we prefer to allocate in a
free pool or to allocate a new pool. This ensures that we only
allocate into pools that do have most of their slots available for
young allocation, which reduces situations where a lot of old values
has to be needlessly scanned by the minor GC.

The rings are managed in such a manner that pools that are less than
half-full are moved to the start of the ring. This ensures that it is
easy to find an old pool for allocation: we only have to look at the
first one.

Care is taken so that programs that do not have any root allocated, do
not pay any of the costs. At each collection, the currently-allocating
pool is reset, so that a domain that does not allocate any root
between two minor collections does not have to scan anything. In
addition, pools that become empty are immediately moved to the free
ring, which is not considered for scanning.

#### Alternative generational optimizations {#sec:alt-gen}

We initially tried several other approaches to make boxroots
"generational", in particular:

1. allocating old values directly into old pools, by classifying the
   values during allocation;

2. adding young roots to OCaml's remembered set (a data structure used
   by OCaml's write barrier), instead of scanning them ourselves
   during minor collections. To avoid repeatedly adding the same root
   to the remembered set in situations like function-local roots, an
   auxiliary freelist is added to each pool, as a cache of
   already-added young roots. (This has been suggested to us by
   Stephen Dolan.)

We realised that in both cases, performing the classification during
scanning instead of allocation results in fewer classifications
performed overall, for a significantly reduced implementation
complexity. We have mentioned above other benefits of doing the
classification during scanning.

### Multicore implementation

In OCaml 4 and earlier, all threads run sequentially by holding a
master lock each in turn. OCaml 5 relaxes this constraint by allowing
several domains to run in parallel. An OCaml domain is a collection of
(system and/or green) threads, running sequentially by holding the
_domain lock_, OCaml 5's per-domain equivalent of the master lock. The
different threads on the same domain share many data structures, most
notably the same minor heap. Programmer are given the advice to
allocate roughly as many domains as CPU cores to use.

OCaml 5.0 introduces a new garbage collector implementation
\citep*{SivaramakrishnanEtAl2020}, with a stop-the-world phase to
collect minor heaps in parallel (similar in this aspect to previous
efforts by \citealp{BourgoinEtAl2020OCaml4Multicore}), and an
implementation of a mostly-concurrent mark & sweep collector for the
major heap. The communication between domains is defined by a new
memory model \citep*{Dolan2018}.

Boxroot for OCaml 5.0 allocates, for each domain, a set of pool rings.
Each domain-local set of pool rings is protected by the local domain
lock. Every allocation is performed in the domain-local pools. As for
deallocations, each one is classified into: _local_, _remote domain_,
_purely remote_. (We have implemented improvements to the OCaml 5.0 C
API to let us perform this classification efficiently.)

- _Local deallocations_ are done on the same domain, while holding the
  domain lock. They are performed immediately without any
  synchronisation being necessary.
- _Remote-domain deallocations_ are those done from a different domain
  than the one that has allocated the boxroot, while holding the
  remote domain's lock. The typical use-case is sending OCaml values
  between threads, or foreign data structures containing such values.
  Since _some_ domain lock is held, we know that no interference with
  scanning is possible. The deallocated root is pushed on a remote
  free-list using two RMW atomic operations (no CAS). The remote free
  list is pushed back on top of the domain-local free list at the
  start of scanning, which takes place during a stop-the-world
  section, when no other remote deallocation can take place.
- _Purely-remote deallocations_ are done without holding the domain
  lock. The typical use-case is for the clean-up of foreign data
  structures that store OCaml values when the domain lock is released
  (e.g. during a panic in Rust). This is rarer, so its performance is
  secondary. To avoid the interference with scanning (without making
  scanning very slow), each pool has a mutex that needs to be locked
  during purely-remote deallocations. This mutex is also locked before
  scanning the pool.^[As an alternative implementation, one could
  avoid locking during purely-remote deallocations by storing the
  metadata about delayed deallocations in a bitmap (taking 32
  additional words in the header for our 16KB pools). By storing this
  metadata separately, one can register remote deallocations in
  parallel with scanning.] In all other aspects, the purely remote
  deallocation is treated like a remote-domain deallocation.

# Benchmarks for Boxroot

<!-- TODO: Guillaume is planning to integrate content on
    the Java comparisons (reap, arenas) in the section.

    TODO: we could evaluate the cost of OCaml->C context switch
    by having a variant of the pure-OCaml implementation that
    in addition performs a no-op (but not no-alloc) C call.
-->

In most cases in current usage, FFI code is not very root-intensive
and the performance impact of rooting is likely to be minor; but for
root-heavy benchmarks, boxroot performs similarly or better than other
mechanisms.

We have devised a few benchmarks to guide us in the implementation of
Boxroot. Three benchmarks compare boxroots to global roots, one
benchmark compares them to local roots. At the time of writing, our
benchmarks are single-threaded, so what is being tested is the
implementation of a concurrent allocator, but without any contention.
We will mention other limitations for each benchmark.

The figures below are obtained with OCaml 4.14 and OCaml 5.0
(development version), on a common commercial laptop with CPU AMD
Ryzen 5850U.

## Global roots benchmarks

<!-- Eww! -->
\vspace{2ex}

\noindent\hfill{}![Global roots benchmarks (OCaml 4.14)](global.pdf){ width=85% }\hfill{}

\vspace{2ex}

\noindent\hfill{}![Global roots benchmarks (OCaml 5.0)](global5.pdf){ width=85% }\hfill{}

\vspace{2ex}

Our benchmarks compare various implementation of an OCaml type
representing a memory cell containing a single value, that has the
same interface as boxroot (`create`, `get`, `delete`, `modify`):

- **OCaml (no boxing)**: a pure OCaml implementation where `create` is
  the identity and nothing happens on deletion. No C call is
  performed.

- **OCaml ref (via C)**: a C implementation of a mutable record (using
  `caml_alloc_small(1,0)`). Deletion is implemented by assigning the
  OCaml integer `0` to the contents. Scanning is done via the value
  being reachable from the OCaml heap.

- **Generational root**: idem, but using malloc and a generational
  global root.

- **Boxroot**: a boxroot, using the implementation we described in
  Section @sec:implem.

- **Doubly-linked list**: a variant of `boxroot`, but using a simpler
  implementation with doubly-linked lists (Section @sec:dll). This
  implementation is not thread-safe: it does not support multiple
  domains nor deletion when the runtime lock is released.

\noindent The various implementations except "OCaml" have similar
memory representation, some being reachable from the OCaml heap and
some outside of the OCaml heap. The ones outside of the OCaml heap are
disguised as immediate values for the OCaml GC, by using a standard
pointer-tagging trick to cast them as values (`(value)p | 1`).

<!--By selecting different implementations of Ref, we can evaluate the
overhead of root registration and scanning for various root
implementations, compared to non-rooting OCaml and C implementations,
along with other factors.-->

We can expect _a priori_ that "OCaml" is always faster, since it has
none of the overheads of other methods. This is meant as a baseline,
but its existence also highlights a limitation of our global roots
benchmarks: "Boxroot" is intended for applications where values are
_not_ easily reachable from the OCaml heap and thus where neither
"OCaml" nor "OCaml ref" are available.

### _Permutations_ benchmark {#sec:bench-perm_count}

The small program used in this benchmark computes the set of all
permutations of the list `[0; ..; `$n$`-1]`, using a non-determinism
monad represented using (strict) lists. (This is an expensive way of
computing $n!$ with lots of allocations.)

In our non-determinism monad, each list element is wrapped as
described above.

This benchmark creates a lot of roots alive at the same time. It
allocates 1860 boxroot pools, and performs 1077 minor and 18 major
collections. Roughly 22M boxroots are allocated in total.

| time in seconds    | OCaml 4.14 | OCaml 5.0 |
|--------------------|-----------:|----------:|
| OCaml              |       1.20 |      1.25 |
| OCaml ref (via C)  |       1.49 |      1.69 |
| Generational roots |       5.90 |      5.43 |
| Doubly-linked list |       2.13 |      1.69 |
| Boxroot            |       1.71 |      1.42 |

We see that generational roots add a large overhead. In OCaml 4.14,
Boxroots out-perform generational global roots, and give slightly
slower time than equivalent pure-GC implementations.

The performance of the simple implementation with doubly-linked lists
is already very good compared to generational roots. However, keep in
mind that this does not take into account the costs of making it
thread-safe. We also believe that the cache-locality-awareness of the
system allocator plays a role in its performance, given that it is the
sole consumer for its allocation size-class (essentially the nodes are
allocated contiguously in memory, using the same memory pool technique
we use for Boxroot). This might not reflect the behaviour of real
workloads.

In OCaml 5.0, the performance gives us a hint of considerations
entering into boxroot's performance. Here Boxroot unexpectedly
outperforms "OCaml ref". Two hypotheses can explain this speedup:

1. Boxroot puts less pressure on the GC. It indeed performs 14% fewer
   minor collections than "OCaml ref".
2. Boxroot has good cache locality during root scanning. Indeed one
   major difference with OCaml 4.14 is the absence of prefetching
   during the major GC. The sensitivity to this effect is confirmed by
   running the benchmark with OCaml 4.12.0, which also lacks
   prefetching and shows Boxroot also outperforming its pure-GC
   counterparts.

### _Synthetic_ benchmark

In this benchmark, we allocate and deallocate values and roots
according to probabilities determined by parameters:

* `N=8`: $\log_2$ of the number of minor generations,
* `SMALL_ROOTS=10_000`: the number of small roots allocated (in the
  minor heap) per minor collection,
* `LARGE_ROOTS=20`: the number of large roots allocated (in the major
  heap) per minor collection,
* `SMALL_ROOT_PROMOTION_RATE=0.2`: the survival rate for small roots
  allocated in the current minor heap,
* `LARGE_ROOT_PROMOTION_RATE=1`: the survival rate for large roots
  allocated in the current minor heap,
* `ROOT_SURVIVAL_RATE=0.99`: the survival rate for roots that survived
  a first minor collection,
* `GC_PROMOTION_RATE=0.1`: promotion rate of GC-tracked values,
* `GC_SURVIVAL_RATE=0.5`: survival rate of GC-tracked values.

These settings favour the creation of a lot of roots, most of which
are short-lived. Roots that survive are few, but they are very
long-lived. The parameters are meant to exaggerate the impact of
Boxroot performance. (In contrast, we are not aware of settings for
which Boxroot would exhibit pathological behaviour.)

| time in seconds    | OCaml 4.14 | OCaml 5.0 |
|--------------------|-----------:|----------:|
| OCaml              |       6.12 |      2.82 |
| OCaml ref (via C)  |       7.15 |      2.98 |
| Generational roots |      10.83 |      4.80 |
| Doubly-linked list |       6.68 |      3.04 |
| Boxroot            |       6.19 |      2.86 |

In OCaml 4.14, this benchmark allocates 772 boxroot pools, and
performs 2,619 minor and 141 major collections. Roughly 16M roots are
allocated.

Boxroot performs better than other root implementations, but it is
unexpected again that it outperforms "OCaml" and "OCaml ref". This is
not well-understood. First, unlike the previous benchmarks, "OCaml
ref" does actually fewer minor and major collection. Second, running
with or without a prefetching GC shows comparable performance between
Boxroot and "OCaml".

In OCaml 5.0, this benchmark allocates 408 boxroot pools, and performs
2,615 minor and 55 major collections. 8.7M roots are allocated.
Therefore results are not comparable between major the old and the new
GC; the absolute speedup is an artifact of our benchmark. The relative
results are similar between OCaml 4.14 and OCaml 5.0, as seen in the
figure.

This synthetic benchmark lets us conclude that there is a large
improvement over generational roots. We do not draw conclusions
regarding its good performance compared to the pure-OCaml version, in
the absence of an interpretation of this result.

### _Globroot_ benchmark

This benchmark is adapted from the OCaml testsuite. It exercises the
case where there are about 1024 concurrently-live roots, but only a
couple of young roots are created between two minor collections.

This benchmark tests the case where there are few concurrently-live
roots and little root creation and modification between two
collections. This benchmark does not perform any OCaml computations or
allocations. It forces collections to occur very often, despite low GC
work. So the costs of root handling and scanning are magnified, they
would normally be amortized by OCaml computations.

In this benchmark there are about 67000 minor collections and 40000
major collections. 217k boxroots are allocated using only 1 pool.

| time in seconds    | OCaml 4.14 | OCaml 5.0 |
|--------------------|-----------:|----------:|
| OCaml              |       0.89 |      0.72 |
| OCaml ref (via C)  |       1.27 |      1.02 |
| Generational roots |       1.14 |      0.92 |
| Doubly-linked list |       0.99 |      0.84 |
| Boxroot            |       1.03 |      0.82 |

Since there are few roots created between collections, list-based
implementations are expected to perform well (their scanning is quick
provided that they implement some form of generational optimisation).
In contrast, Boxroot has to scan on the order of a full memory pool at
every minor collection even if there are only a few young roots, while
our pool size is chosen large (2032 slots).

There used to be a noticeable overhead in this benchmark compared to
list-based implementations (generational roots, doubly-linked lists,
and remembered-set-based boxroots). The overhead _per minor
collection_ was negligible (~2µs). But we have also reduced it by
optimizing the scanning during minor collection: if we know that only
young values must be scanned, then we can test by ourselves whether
values are young, before applying the scanning action to them. There
is a very efficient way of doing this test in a loop. This
optimization brought a speed-up of up to $3.8\times$ to the scanning
of pools that contain very few young values ($2.6\times$ in this
benchmark). The overhead per minor collection is now difficult to
distinguish from fluctuations.

The relative results are similar between OCaml 4.14 and OCaml 5.0.

## Global roots: multicore scaling benchmark {#sec:global-scaling-bench}

<!-- TODO(Gabriel): Update with 8-core results.

We run on OCaml 5.1 and yet we observe that boxroot is faster than calling the GC through C.

This is not due to prefeteching, as OCaml 5.1 has prefixing and the
pure-OCaml benchmark is competitive with boxroot.

Hypothesis: we are observing the cost of calling a C function, which
is higher in Multicore than in OCaml 4.

Control (TODO): call a dummy C functions from the native-OCaml implementation.
-->

\newcommand{\DOMS}{$\mathtt{DOMS}$}
\newcommand{\ix}{$\times$}

We claim that the boxroot API is more amenable than OCaml's current
global root registration APIs to implementations that scale well in
multicore setting. To validate this, we implemented a multicore
version of the Permutations benchmark of Section
@sec:bench-perm_count.

The benchmark code is the same as the _Permutations_ benchmark of
Section @sec:bench-perm_count; in particular, we use boxroots or
global roots in C code that is actually called from OCaml, and it is
possible to replace those roots (which are explicitly allocated
and deallocated) by an OCaml reference handled by the OCaml GC. The
implementations compared are the following:

- **ref (native)**: using an OCaml reference directly in OCaml
  (we expect this to be the fastest version).
- **boxroot**: our boxroot implementation.
- **ref (via C)**: uses an OCaml reference allocated from C; this
  implementation should behave exactly like **ref (native)**,, except
  for the FFI overhead.
- **globroots (mutex)**: uses malloc and a gnerational global root as
  implemented in the OCaml 5.0 runtime. The implementation uses
  a skiplist protected by a global mutex.
- **globroots (lock-free)**: we implemented a variant of OCaml's
  generational global roots using a lock-free skiplist for better
  scalability.\footnote{This is a relatively simple change as there is
  already an implementation of a lock-free skiplist in the 5.0 OCaml
  runtime, used for other purposes.}

Our benchmark runs `DOMS` instances of the exact same workload in parallel
on `DOMS` domains. this is embarrassingly parallel, with no synchronization
in the benchmark code. In an ideal world, running the benchmark should
take the exact same time as the sequential or `DOMS`=1 version of the
benchmark, as long as the machine has more than `DOMS` cores available for
computation. Any increase in real time comes from concurrency
bottlenecks in the OCaml runtime and our boxroot implementation.

### Qualitative results

Our benchmark results are given below. We first plot the the two
salient metrics: real time, and parallel speedup, that is, how much
faster each run is than running `DOMS` workloads sequentially on
a single core.

\noindent\hfill{}![Multicore scaling: real time](par_perm_count-real_time.pdf){ width=85% }\hfill{}

\noindent\hfill{}![Multicore scaling: parallel spedup](par_perm_count-parallel_speedup.pdf){ width=85% }\hfill{}

The qualitative results visible on this plots are the following:

1. Boxroot behaves essentially like the GC implementations -- either
   the native OCaml benchmarks or when allocating via C.

2. The existing global roots API performs much worse than boxroot,
   already on sequential benchmarks as discussed previously, but the
   gap widens considerably on multicore benchmarks. This is due to the
   fact that the existing global roots implementations behave as
   sequential bottlenecks.

3. The "good" implementations on this benchmark do not scale all that
   well, with approximately a 4\ix speedup on 8 cores. This is an
   embarrassingly parallel problem so one may expect a 8\ix
   speedup. We believe that this comes from the stop-the-world nature
   of minor collections in the parallel OCaml runtime, which limit
   scalability for GC-bound programs -- this benchmark is designed to
   stress the GC, so it probably fares worse than typical OCaml
   programs. In any case, this disappointing scalability is *not*
   caused by our boxroot implementation, as the native OCaml version
   gets the same speedups.

In other words, these results confirm our claim that boxroots scale
well, at least up to 8 cores, much better than generational root --
either the current implementation or our variant optimized for better
scaling.

### Detailed results

Here are complete tables with a set of interesting metrics.
(We omitted the data for 5 and 7 domains in the interest of presentiation space.)

\newcommand{\hide}[1]{}

#### GC-ed references (native, or via C)

<!-- Gabriel: if you touch the tables in this section, you may have to
    adjust the hackish 'sed' call in the Makefile to prevent line
    wrapping in the first column. -->

| ref (native)      | `DOMS`=1                  | `DOMS`=2                  | `DOMS`=3                  | `DOMS`=4                  | `DOMS`=6                  | `DOMS`=8                  |
|-------------------|---------------------------|---------------------------|---------------------------|---------------------------|---------------------------|---------------------------|
| real time         | 0.603s \hide{$\pm$ 0.012} | 0.668s \hide{$\pm$ 0.096} | 0.716s \hide{$\pm$ 0.028} | 0.838s \hide{$\pm$ 0.058} | 1.003s \hide{$\pm$ 0.084} | 1.319s \hide{$\pm$ 0.040} |
| slowdown vs. refs | 1\ix                      | 1\ix                      | 1\ix                      | 1\ix                      | 1\ix                      | 1\ix                      |
| user time         | 0.57s                     | 1.17s                     | 1.78s                     | 2.70s                     | 4.56s                     | 8.17s                     |
| parallel speedup  | 1\ix                      | 1.80\ix                   | 2.53\ix                   | 2.88\ix                   | 3.61\ix                   | 3.66\ix                   |

| ref (via C)       | `DOMS`=1                   | `DOMS`=2                   | `DOMS`=3                   | `DOMS`=4                 | `DOMS`=6                  | `DOMS`=8                  |
|-------------------|----------------------------|----------------------------|----------------------------|--------------------------|---------------------------|---------------------------|
| real time         | 0.793s \hide{$\pm$ 0.0235} | 0.848s \hide{$\pm$ 0.0184} | 0.962s \hide{$\pm$ 0.0715} | 1.016s \hide{$\pm$ 0.05} | 1.200s \hide{$\pm$ 0.072} | 1.646s \hide{$\pm$ 0.118} |
| slowdown vs. refs | 1.31\ix                    | 1.27\ix                    | 1.34\ix                    | 1.21\ix                  | 1.2\ix                    | 1.25\ix                   |
| user time         | 0.75s                      | 1.51s                      | 2.46s                      | 3.38s                    | 5.58s                     | 10.51s                    |
| parallel speedup  | 1\ix                       | 1.87\ix                    | 2.47\ix                    | 3.12\ix                  | 3.96\ix                   | 3.85\ix                   |

The cost of going through the C FFI is noticeable on this benchmark:
the via-C version is 30\% slower in the sequential case. The extra
time spent in the FFI does not incur any synchronization, so it
parallelizes well and the C version appears to scale slightly better.

#### Boxroot

| Boxroot           | `DOMS`=1                   | `DOMS`=2                  | `DOMS`=3                  | `DOMS`=4                  | `DOMS`=6                  | `DOMS`=8                 |
|-------------------|----------------------------|---------------------------|---------------------------|---------------------------|---------------------------|--------------------------|
| real time         | 0.690s \hide{$\pm$ 0.0230} | 0.766s \hide{$\pm$ 0.057} | 0.768s \hide{$\pm$ 0.032} | 0.891s \hide{$\pm$ 0.049} | 1.015s \hide{$\pm$ 0.060} | 1.350 \hide{$\pm$ 0.017} |
| slowdown vs. refs | 1.14\ix                    | 1.15\ix                   | 1.07\ix                   | 1.06\ix                   | 1.01\ix                   | 1.02\ix                  |
| user time         | 0.65s                      | 1.34s                     | 1.96s                     | 2.95s                     | 4.90s                     | 8.63s                    |
| parallel speedup  | 1\ix                       | 1.80\ix                   | 2.69\ix                   | 3.10\ix                   | 4.08\ix                   | 4.09\ix                  |

When the number of domains is low, boxroots are about 15\% slower than
native OCaml references. This overhead becomes smaller as the core
count augments, and it is only 1% or 2% with 7 or 8 cores. This comes
from the fact that the boxroot version scales better than the native
OCaml version.

We believe that this better scaling comes from the fact that the GC
handles a smaller fraction of the program memory, and that this
reduces the impact of concurrency bottlenecks coming from the minor
GC.

#### Generational global roots

| globroots (lock-free) | `DOMS`=1                   | `DOMS`=2                   | `DOMS`=3                   | `DOMS`=4                   | `DOMS`=6                   | `DOMS`=8                   |
|-----------------------|----------------------------|----------------------------|----------------------------|----------------------------|----------------------------|----------------------------|
| real time             | 7.157s \hide{$\pm$ 0.118 } | 11.432s \hide{$\pm$ 0.136} | 13.305s \hide{$\pm$ 0.567} | 18.252s \hide{$\pm$ 0.532} | 25.449s \hide{$\pm$ 0.402} | 36.837s \hide{$\pm$ 0.459} |
| slowdown vs. refs     | 11.87\ix                   | 17.11\ix                   | 18.59\ix                   | 21.76\ix                   | 25.37\ix                   | 27.93\ix                   |
| user time             | 7.06s                      | 19.93s                     | 32.89s                     | 55.44s                     | 112.84s                    | 197s                       |
| parallel speedup      | 1\ix                       | 1.25\ix                    | 1.61\ix                    | 1.57\ix                    | 1.69\ix                    | 1.55\ix                    |

| globroots (mutex)     | `DOMS`=1                   | `DOMS`=2                   | `DOMS`=3                   | `DOMS`=4                   | `DOMS`=6                  | `DOMS`=8                    |
|-----------------------|----------------------------|----------------------------|----------------------------|----------------------------|---------------------------|-----------------------------|
| real time             | 4.751s \hide{$\pm$ 0.025 } | 21.823s \hide{$\pm$ 0.545} | 28.199s \hide{$\pm$ 1.291} | 42.751s \hide{$\pm$ 1.533} | 79.83s \hide{$\pm$ 2.563} | 125.016s \hide{$\pm$ 2.214} |
| slowdown vs. refs     | 7.88\ix                    | 32.65\ix                   | 39.4\ix                    | 50.98\ix                   | 79.59\ix                  | 94.78\ix                    |
| user time             | 4.68s                      | 22.81s                     | 34.01s                     | 55.43s                     | 111.33s                   | 189.17s                     |
| system time (% total) | 0.6%                       | 44.6%                      | 52.4%                      | 60%                        | 70%                       | 75%                         |
| parallel speedup      | 1\ix                       | 0.44\ix                    | 0.51\ix                    | 0.44\ix                    | 0.36\ix                   | 0.3\ix                      |

Our implementation of generational global roots using a lock-free
skiplist scales noticeably better than the one using a global mutex,
of course. For `DOMS=4`, the **globroots (mutex)** benchmark spends
65\% of its cpu time in system time, corresponding to synchronization
overheads -- whereas the system time of all other implementations is
neglectible, below 1\%.

But the lock-free version has worse single-core performance. This
single-core baseline is the reason why the upstream 5.0 OCaml runtime
uses a global mutex: most real-world OCaml programs remain purely
sequential, so improving scalability at the cost of sequential
performance is not necessarily the right decision for the runtime
system.

#### Caveats

For simplicity we have only performed our tests on a small number of
cores (at most `DOMS`=8), as can easily be found on commodity
hardware. This experiment does not demonstrate that boxroot scales
well to a large number of cores.

One should also keep in mind that our benchmark is designed to stress
the root-reservation implementation, and spends a much larger amount
of time reserving roots than most FFI code. This magnifies performance
differences which would be much smaller for many (but not all)
use-cases.

## Local roots benchmark {#sec:local-bench}

\noindent We designed this benchmark to test the idea of replacing local
roots altogether with boxroots.

Currently, OCaml FFI code uses a "callee-roots" discipline where each
function has to root each local OCaml value in use, using the
efficient `CAMLparam`, `CAMLlocal`, `CAMLreturn` macros. These macros
manage a shadow stack of roots pointing to function arguments and
local variables. (Section @sec:local-roots)

Boxroot proposes a "caller-roots" approach where callers package their
OCaml values in boxroots, following an ownership discipline (they can
be passed to the callee owned or borrowed). Creating boxroots is
slower than registering local roots, but the caller-roots discipline
can avoid re-rooting each value when moving up and down the call
chain, so it is expected to have a performance advantage for deep call
chains.

This benchmark performs a (recursive) fixpoint computation on OCaml
floating-point values, with a parameter $N$ that decides the number of
fixpoint iterations needed, and thus the length of the C-side call
chain. Essentially:

```ocaml
let rec fixpoint f x =
  let y = f x in
  if 0 = Float.compare x y then y
  else ocaml_fixpoint f y

let () =
  for _i = 1 to num_iter do
    ignore (fixpoint (fun x -> if truncate x >= n then x else x +. 1.) 1.)
  done;
```

\noindent The time is then normalised to show the average duration of a single
recursive call.

- **OCaml**: the purely-OCaml function `fixpoint` above, no C calls.

- **OCaml ref**: same, but adding one level of indirection for each
  argument, simulating Boxroot's memory layout.

- **Local roots**: calls the `local_fixpoint` function given in
  Section @sec:local-roots. It is the C equivalent of `fixpoint`,
  following the documented OCaml-C style using local roots. In
  addition, `compare_val` compares the values of `x` and `y`, but
  introduces local roots in order to simulate a more complex
  computation.

- **Boxroot**: the Boxroot version is as follows:

  ```c
  int compare_refs(value const *x, value const *y);

  boxroot boxroot_fixpoint_rooted(value const *f, boxroot x)
  {
    boxroot y = boxroot_create(caml_callback(*f, boxroot_get(x)));
    if (compare_refs(boxroot_get_ref(x), boxroot_get_ref(y))) {
      boxroot_delete(x);
      return y;
    } else {
      boxroot_delete(x);
      return boxroot_fixpoint_rooted(f, y);
    }
  }
  ```
  \noindent where `compare_refs` does the same work as `compare_val`
  but expects its values already rooted. The function
  `boxroot_fixpoint_rooted` takes two roots as argument: the first one
  is borrowed, whereas the second one is taken ownership of.

  The work is done by `boxroot_fixpoint_rooted`, but we need a
  `boxroot_fixpoint` wrapper to go from the callee-roots convention
  expected by the OCaml-C FFI, to a caller-root convention.

  ```c
  value boxroot_fixpoint(value f, value x)
  {
    boxroot f_root = boxroot_create(f);
    boxroot x_root = boxroot_create(x);
    boxroot y = boxroot_fixpoint_rooted(boxroot_get_ref(f), x_root);
    value v = boxroot_get(y);
    boxroot_delete(y);
    boxroot_delete(f_root);
    return v;
  }
  ```
  \noindent This wrapper adds some overhead for small call depths.

- **Boxroot (callee-roots)**: uses boxroots, but using the same
  callee-roots discipline as local roots. This control lets us
  separate the gains due to the differences between rooting protocols
  from the more measurable performance costs.

- **Generational roots**: same as Boxroot, but using
  malloc+generatinal roots instead.

We also show earlier experiments that did not implement thread-safety:

- **dll_boxroot**: the implementation of boxroots using doubly-linked
  lists,

- **rem_boxroot**: a version of `boxroot` which implements a
  generational strategy based on OCaml's remembered set (Section
  @sec:alt-gen). It also lacks various optimisations, such as the
  inlining of the fast paths of `boxroot_create` and `boxroot_delete`.

### In OCaml 4.14

\noindent\hfill{}![Local roots benchmarks (OCaml 4.14)](local.pdf){ width=95% }\hfill{}

We see that, in this test, despite the up-front cost of wrapping the
function, boxroots are equivalent to or outperform OCaml's local
roots. More precisely, boxroots are slightly more expensive than local
roots when following the same callee-roots discipline, and the
caller-roots discipline offers huge saves in this benchmark. Analysing
the generated assembly of the fixpoint, the saves from the
caller-roots discipline come from:

- introducing fewer roots,
- enabling recursion to be done via a tail call,
- enabling better code generation after inlining.

\noindent Thus the gains from the caller-roots discipline compared to
callee-roots greatly depends on the program and programming style. (In
this particular program, "expert" usage of local roots in use does not
improve performance.)

<!-- Note: removed paragraph. I tried, and getting it to be tail
recursive is very clumsy and not so beneficial.

(We did not take into account local roots
optimisations that fall outside of the documented syntactic rules of
local roots usage, and require expert knowledge of their
implementation. For instance it is possible with local roots, but not
documented nor well-known, to rewrite `local_fixpoint` in a
tail-recursive manner. In contrast, the optimisations permitted by the
caller-roots protocol of Boxroot are intended to be idiomatic to Rust
programmers and entirely enabled and made safe by Rust's type system.)

-->

Our conclusions:

- Using boxroots is competitive with local roots.
- It can be beneficial if one leverages the added flexibility of
  boxroots.
- There could be specific scenarios where it is much more beneficial,
  for instance when traversing large OCaml structures from a foreign
  language, with many function calls.

Furthermore, we envision that with support from the OCaml compiler for
the caller-roots discipline, the wrapping responsible for the initial
overhead could be made unnecessary. The compiler could instead pass
roots located on the OCaml stack.

### In OCaml 5.0

\noindent\hfill{}![Local roots benchmarks (OCaml 5.0)](local5.pdf){ width=95% }\hfill{}

In multicore OCaml, we observe similar results. However, the overhead
of C calls appears to be significantly higher, and generational roots
are much more expensive (for two reasons probably: they are now
protected by a mutex, and the runtime makes greater use of them
internally, which imposes an overhead due to the logarithmic
complexity).

Next we have measured the performance of two alternative
implementations:

- **Boxroot (thread-unsafe)**: assumes that there is only one thread
  that never releases the domain lock, and thus avoids related checks
  in `boxroot_create` and `boxroot_delete`.
- **Boxroot (force remote)**: the opposite, performs all deallocations
  as if they were done on a different domain, using the lock-free
  atomic deallocation path.

\noindent ![Local roots benchmarks (impact of multicore
support)](local5-2.pdf)

Our thread-safe implementation of Boxroot for multicore OCaml is
slightly slower than a version that does not perform checks necessary
for thread-safety. The difference is likely less important in other
kinds of situations where more time is spent in cache misses.

The implementation where all deallocations are done remotely is only
slightly slower than Boxroot (although with a much higher pool count
currently, due to the fact that flushing the remote free-list of each
pool is delayed until the next garbage collection). However this
single-threaded benchmark does not let us see the costs of cache
effects in realistic multithreaded scenarios (cache misses and
contention).

Our conclusions for the multicore implementation:

- The overhead of multithreading support is low enough to propose
  Boxroot as an all-purpose rooting mechanism. (An alternative would
  be to introduce a specialised version for the case of local roots,
  which could still be done at some later point. However if
  micro-optimisations become necessary, the caller-roots discipline
  already provides plenty of ways of avoiding rooting altogether.)
- The performance of cross-thread deallocation is likely very good,
  but we need better benchmarks to measure this.

# TODO move elsewhere

<!-- TODO: Fix, remove, relocate?

    What remains below is waiting to be moved somewhere else.
-->

### Boxroot vs. global roots

Boxroot can be implemented on top of global roots in OCaml 5 by using
`malloc` as we mentioned, which combines an OCaml-agnostic allocator
with an address-agnostic GC-registration data-structure, protected by
a global lock. But we show that large efficiency gains are possible by
integrating the allocator with the GC-registration logic:

- operations can have constant time complexity,
- the implementation can be more multicore-friendly,
- scanning can be done in all domains in parallel,
- data structures can have better temporal and spatial cache locality,
  benefiting both scanning and the operations.

\noindent In fact, the good performance lets us propose it as a single
rooting mechanism replacing both global and local roots.

# Discussion and Conclusion

## Discussion: mixing linear and non-linear values {#sec:linear-allocation}

<!-- TODO gm: ailleurs? -->

We believe that our results go beyond the matter of safe and
performant FFI. Another use case for a boxroot-like design is to sit
at the boundary between two different regions of memory, in the same
language, one storing linearly-allocated values, handled with explicit
resue or deallocation, and the other storing non-linear values,
managed by a GC.

Any reference from the linear region to the non-linear region would
need to be accounted for as a root, and our experiment gives an idea
of theoretical costs of embedding (owning) a GC-allocated value inside
a linearly-allocated value. As a rule of thumb:

- The performance of allocating and deallocating a boxed root should
  be seen as similar to that of allocating and deallocating
  linearly-allocated values with a modern concurrent allocator,
- The performance of scanning boxed roots during GC should be seen as
  similar or better to that of scanning the same values if they were
  reachable from the GC heap, thanks to good memory locality.

\noindent In qualitative terms of performance, this notion of root is
"zero-cost" (it does not create costs that are not already there),
making it a suitable device for consideration in proposed language
explorations in mixing linear allocation and tracing
GCs \citep{Munch2023,Munch2018RePo}.

## Conclusion

<!-- ## What -->

1. We proposed a new data abstraction of the notion of GC root, boxroots.
   They serve the interface between values managed by a GC and a host
   language.

   Boxroots are resources which follow an ownership discipline. They can
   be passed to functions by ownership or by borrowing, they can be
   returned, inserted in foreign data structures, and sent and shared
   across threads.

<!-- ## Why -->

2. Boxroots support the safe manipulation of GC values from Rust,
   extending previous investigations by \citet*{josephine, caml-oxide}.
   We presented a caller-roots discipline suitable for dealing with a
   GC in Rust, which supports passing rooted values both by ownership
   and by borrow, as well as avoiding unnecessary rooting when a value
   is static or when a function does not allocate---all thanks to some
   polymorphism and distinctions made via the lifetime discipline of
   Rust. A rooting mechanism such as Boxroot is then essential to
   manipulate GC values under this discipline.

   Our Boxroot library became quickly used in several Rust libraries
   implementing OCaml-Rust interfaces.

   During our research, we have encountered several limitations of
   Rust's type system, which are well-known but appear all the more
   important here. We hope that future improvements of the Rust type
   system—like it has happened in the past—can make our discipline
   simpler to use.

<!-- ## How -->

3. We presented an implementation of boxroots that performs vastly
   better than OCaml's generational roots, and significantly better
   than a naive implementation based on doubly-linked lists. The
   implementation is also competitive with OCaml's local roots, but
   more expressive than those. Notably, the gain in expressivity can
   translate in further performance gains (caller-roots vs.
   callee-roots protocols).

   Boxroot relies on various implementation techniques, some from
   modern allocators, some from GCs (generational optimisations). It
   draws expected benefits from modern concurrent allocator
   technology: cache locality, multicore-friendliness. The cost of
   making it thread-safe is acceptable. The implementation of
   cross-thread deallocation is simplified by assumptions made by the
   garbage collector (regular stop-the-world phases).

   The cost of allocating a boxroot should be expected to be on par
   with modern concurrent allocators, the cost of scanning the roots
   on par with modern garbage collectors. This suggests that phase
   transitions from linearly allocated values to GC-allocated values
   is without overhead (zero-cost heuristic for mixing linear and GC
   allocation).

   The good performance results let us propose Boxroot as a single
   all-purpose rooting mechanism (at least in the absence of a deeper
   integration of the GC in the host language: Section @sec:santa).

   Being multi-threading-capable is not only a matter of performance
   but also of resource safety. Indeed, since one purpose of boxroots
   is to be stored in foreign data structures, it appears essential
   that such resources are ready to be cleaned-up anytime. Deleting a
   boxroot should not require unrealistic constraints, such as
   acquiring OCaml's runtime lock.

<!-- ## Limitations -->

4. We have not proposed benchmarks to evaluate latency. Indeed we have
   not implemented an incremental scanning of roots due to limitations
   of the hooks we use to communicate with the OCaml runtime. This
   limitation could be lifted after integrating our library with the
   OCaml runtime, or after making the hooks interface more expressive.

   The benefits in terms of alleviating GC pressure should be further
   investigated. This would require benchmarks that would move
   significantly more allocations outside of the GC heap, of values
   that would normally be promoted.
