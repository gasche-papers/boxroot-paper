LATEXMK=latexmk -pdf -bibtex

.PHONY: all
all: workshop.pdf

workshop.pdf: workshop.tex boxroot.bib
	$(LATEXMK) workshop.tex

# Note: the latest release of pandoc-secnos does not support pandoc 3.x,
# see https://github.com/tomduck/pandoc-xnos/issues/28
# A temporary workaround is to install the following fork instead
#   pip install git+https://github.com/TimothyElder/pandoc-xnos

workshop.tex: workshop.md
	pandoc workshop.md workshop-old.md --filter pandoc-secnos --standalone -o workshop.tex
	sed 's/real{0.\(1050\|1038\|1204\)}/real{0.3000}/' -i workshop.tex
# 	The 'sed' call rewrites the width of the first column of the detailed benchmark tables, to avoid having the metric descriptions wrapped in several lines. If you modify the benchmark tables, you may have to adjust the script here with new dimensions.

%.pdf: %.svg
	inkscape --export-type pdf $<

.PHONY: clean
clean:
	$(LATEXMK) -c
	rm workshop.tex
